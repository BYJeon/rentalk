//
//  UnderLineTextFieldProtocol.swift
//  rentalk
//
//  Created by Yuna Daddy on 18/06/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import Foundation

@objc protocol UnderLineTextFieldProtocol {
    func textValueChanged(sender: UnderlineTextField)
    func textValueDidBegin(sender: UnderlineTextField)
    func textShouldReturn(sender: UnderlineTextField)
    @objc optional func textValueCleared(sender: UnderlineTextField)
}

class UnderlineTextField: UITextField, UITextFieldDelegate {
    @IBInspectable var focusedColor : UIColor = UIColor.gray
    @IBInspectable var normalColor : UIColor?{
        didSet{
            border.borderColor = normalColor?.cgColor
        }
    }
    
    var delegateTextField: UnderLineTextFieldProtocol?
    
    let border = CALayer()
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        createBorder()
        self.delegate = self
        
    }
    
    
    func createBorder(){
        let width = CGFloat(2.0)
        
        border.borderColor = UIColor(red: 55/255, green: 78/255, blue: 95/255, alpha: 1.0).cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height-width, width: self.frame.size.width*2, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        //print("border created")
    }
    
    func pulseBorderColor(){
        border.borderColor = focusedColor.cgColor
    }
    
    func normalBorderColor(){
        border.borderColor = normalColor?.cgColor
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        pulseBorderColor()
        //TextField 값 변화를 감지함.
        self.delegateTextField?.textValueDidBegin(sender: self)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        normalBorderColor()
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        //TextField 값 변화를 감지함.
        //self.delegateTextField?.textValueCleared!(sender: self)
        textField.text = ""
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //TextField 값 변화를 감지함.
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(self.getHintsFromTextField),
            object: textField)
        self.perform(
            #selector(self.getHintsFromTextField),
            with: textField,
            afterDelay: 0.5)
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        self.delegateTextField?.textShouldReturn(sender: self)
        return false
    }
    
    @objc func getHintsFromTextField(textField: UITextField) {
        print("Hints for textField: \(textField)")
        self.delegateTextField?.textValueChanged(sender: self)
    }
}
