//
//  CheckBox.swift
//  rentalk
//
//  Created by Yuna Daddy on 18/06/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

class CheckBox: UIButton {
    
    // Images
    let checkedImage = UIImage(named: "btn_check_p")! as UIImage
    let uncheckedImage = UIImage(named: "btn_check_n")! as UIImage
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setBackgroundImage(checkedImage, for: .normal)
            } else {
                self.setBackgroundImage(uncheckedImage, for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action: #selector(CheckBox.buttonClicked), for:.touchUpInside)
        self.isChecked = false
    }
    
    @objc dynamic private func buttonClicked(sender: CheckBox) {
        if sender == self {
            isChecked = !isChecked
        }
    }
}
