//
//  DriverInfo.swift
//  rentalk
//
//  Created by Yuna Daddy on 06/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import Foundation

class DriverInfo{
    var dist_gap: String = ""
    var driver_nm: String = ""
    var start_point_la: String = ""
    var start_point_lo: String = ""
    
    var end_point_la: String = ""
    var end_point_lo: String = ""
    
    var way_point_la: String = ""
    var way_point_lo: String = ""
    
    var eval_score: String = ""
    var profile_photo: String = ""
    var expected_amt: String = ""
    var driver_id: String = ""
    var missions: [String] = []
    var dirver_image: UIImage?
    
    init(driverID: String, driver_nm: String, dist_gap: String, missions: [String], expected_amt: String, profile_photo: String, eval_score: String,
         start_point_la: String, start_point_lo: String, end_point_la: String, end_point_lo: String, waypoint_la: String, waypoint_lo: String) {
        self.driver_id = driverID
        self.driver_nm = driver_nm
        self.dist_gap = dist_gap
        self.driver_id = driverID
        self.missions = missions
        self.expected_amt = expected_amt
        self.profile_photo = profile_photo
        self.eval_score = eval_score
        
        self.start_point_la = start_point_la
        self.start_point_lo = start_point_lo
        
        self.end_point_la = end_point_la
        self.end_point_lo = end_point_lo
        
        self.way_point_la = waypoint_la
        self.way_point_lo = waypoint_lo
    }
}
