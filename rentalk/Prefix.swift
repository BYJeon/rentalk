//
//  Prefix.swift
//  rentalk
//
//  Created by Yuna Daddy on 16/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit


public var USE_INDEX: Bool = false                  // true - index / false - main


//public let API_SERVER: String                               = "http://52.79.48.82:8000/api/v1"
public let API_SERVER: String                               = "https://api.rentalk.co.kr/api/v1"

public let URL_WEB_SHOP: String                             = "http://shop.minetalk.co.kr/m/?shop_auth=%@"          //쇼핑몰 도메인

//public let API_SERVER: String                               = "http://tapp.minetalk.co.kr"
//public let URL_WEB_SHOP: String                             = "http://tshop.minetalk.co.kr/m/?shop_auth=%@"          //쇼핑몰 도메인

public let API_APP_VERSION                                  = "/api/app/version"            // 버전체크

public let API_USER_REGIST_REQ_NUM                          = "/auth/employee/join/auth_no"  // 회원가입 인증요청
public let API_USER_LOGIN_REQ_NUM                           = "/auth/employee/login/auth_no" // 로그인 인증요청

public let API_USER_REGIST                                  = "/auth/employee/join"  // 회원가입
public let API_USER_LOGIN                                   = "/auth/employee/login" // 로그인 인증요청
public let API_DRIVER_REQUEST                               = "/employee/driver-request" // 탁송 요청
public let API_DRIVER_REQUEST_CANCLE                        = "/employee/driver-request" // 탁송 요청 취소
public let API_APP_STATUS_REQUEST                           = "/employee/app-status"    // 앱 상태 요청


public let API_DRIVER_INFO_REQUEST                          = "/employee/driver-request/driver" // 드라이버 정보 요청
public let API_DRIVER_END_INFO_REQUEST                      = "/employee/driver-request/end"   // 탁송 완료 정보 요청
public let API_DRIVER_RESULT_REQUEST                        = "/employee/driver-request/list"  // 탁송 정보 요청
public let API_DRIVER_CONTRACT_REQUEST                      = "/contract"  // 계약서 조회
public let API_DRIVER_RIDING_LOCATION_REQUEST               = "/employee/driver-request/riding-location"  // 드라이버 정보 요청

public let API_SLACK_CUSTOMER_REQUEST                       = "https://hooks.slack.com/services/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"

public let API_USER_SNS_CHECK                               = "/api/user/sns_check"         // sns 가입여부 체크
public let API_USER_INFO                                    = "/api/user/info"              // 사용자 정보 조회
public let API_USER_PWINQUIRY                               = "/api/user/pwinquiry"         // 비밀번호 찾기
public let API_USER_MODIFY_USER_NAME                        = "/api/user/modify_user_name"          // 회원 이름 변경
public let API_USER_MODIFY_USER_BIRTHDAY                    = "/api/user/modify_user_birthday"      // 회원 생년월일 변경
public let API_USER_MODIFY_USER_EMAIL                       = "/api/user/modify_user_email"         // 회원 이메일 변경
public let API_USER_MODIFY_USER_ADDR                        = "/api/user/modify_user_addr"          // 회원 주소 변경
public let API_USER_MODIFY_USER_PWD                         = "/api/user/modify_user_pwd"           // 회원 비밀번호 변경
public let API_USER_MODIFY_USER_STATE_MESSAGE               = "/api/user/modify_user_state_message"             // 회원 상태메세지 변경
public let API_USER_SET_MESSAGE_RECEIVE_FLAG                = "/api/user/set_message_receive_flag"              // 회원 메세지 수신여부 설정
public let API_USER_SECESSION_USER                          = "/api/user/secession_user"                        // 회원 탈퇴
public let API_USER_MODIFY_USER_GENDER                      = "/api/user/modify_user_gender"                    // 회원 성별 변경
public let API_USER_MODIFY_USER_JOB                         = "/api/user/modify_user_job"                       // 회원 직업 변경
public let API_USER_UPLOAD_CONTACT                          = "/api/user/upload_contact"            // 연락처 업로드
public let API_USER_LOGOUT                                  = "/api/user/logout"                    // 로그아웃
public let API_USER_LIST_RECOMMEND                          = "/api/user/list_recommend"            // 추천인 리스트 조회


public let API_COMMUNITY_LIST_QNA                           = "/api/community/list_qna"             // 문의하기 리스트
public let API_COMMUNITY_ADD_QNA                            = "/api/community/add_qna"              // 문의하기 등록
public let API_COMMUNITY_LIST_FAQ                           = "/api/community/list_faq"             // FAQ 리스트
public let API_DIRVER_EVALUATE                           = "/employee/employeeEvaluate"           // 드라이버 평가


/* 채팅방 리스트 삭제 - 테스트용 */
public let API_USER_DELETE_DATA                             = "/api/user/delete_data"


public let API_FRIEND_ADD_FRIEND                            = "/api/friend/add_friend"              // 친구 추가
public let API_FRIEND_REMOVE_FRIEND                         = "/api/friend/remove_friend"           // 친구 삭제
public let API_FRIEND_SET_REJECT_FRIEND                     = "/api/friend/set_reject_friend"       // 친구 차단 설정
public let API_FRIEND_LIST_FRIEND                           = "/api/friend/list_friend"             // 친구 리스트
public let API_FRIEND_INFO                                  = "/api/friend/info"                    // 친구 정보 조회
public let API_FRIEND_GET_FRIEND_CONFIG                     = "/api/friend/get_friend_config"       // 친구 설정 가져오기
public let API_FRIEND_SET_FRIEND_CONFIG                     = "/api/friend/set_friend_config"       // 친구 설정 변경
public let API_FRIEND_FIND_FRIEND                           = "/api/friend/find_friend"             // 친구 찾기

public let API_POINT_LIST_POINT                             = "/api/point/list_point"               // 마인 캐시/토큰 내역
public let API_POINT_GIFT_POINT                             = "/api/point/gift_point"               // 마인 캐시/토큰 선물하기
public let API_POINT_GIFT_POINT_V2                          = "/api/point/gift_point_v2"
public let API_POINT_PAYMENT_POINT                          = "/api/point/payment_point"            // 마인 캐시/토큰 결제하기
public let API_POINT_CHARGE_POINT                           = "/api/point/charge_point"             // 마인 캐시 충전하기
public let API_POINT_GET_CHARGE_CONFIG                      = "/api/point/get_charge_config"        // 충전하기 설정조회
public let API_POINT_GET_CHARGE_CONFIG_V2                   = "/api/point/get_charge_config_v2"     // 충전하기 설정조회_V2

public let API_TRANSLATE_GOOGLE_TRANSLATE                   = "/api/translate/google_translate"           // 구글번역
public let API_TRANSLATE_GOOGLE_OCR_TEXT_DETECTION          = "/api/translate/google_ocr_text_detection"  // 구글 OCR 텍스트 추출

public let API_COMMON_REQUEST_SMS                           = "/api/common/request_sms"             // 인증번호 요청
public let API_COMMON_REQUEST_ADDR                          = "/api/common/request_addr"            // 주소 정보 조회
public let API_COMMON_LIST_POLICY                           = "/api/common/list_policy"             // 약관 및 규정

public let API_UPLOAD_PICTURE                               = "/api/upload/picture"                 // 프로필 사진 업로드
public let API_UPLOAD_AD_IMAGE                              = "/api/upload/ad_image"                // 광고 이미지 업로드
public let API_UPLOAD_MESSAGE_FILE                          = "/api/upload/message_file"            // 메세지 파일 업로드

public let API_COMMUNITY_LIST_NOTICE                        = "/api/community/list_notice"          // 공지사항 리스트

public let API_AD_LIST_AD_MESSAGE                           = "/api/ad/list_ad_message"             // 광고 리스트 조회
public let API_AD_TARGET_COUNT                              = "/api/ad/target_count"                // 광고 수신 대상 회원수 조회
public let API_AD_SEND_AD_MESSAGE                           = "/api/ad/send_ad_message"             // 광고 메세지 작성
public let API_AD_VIEW_AD_MESSAGE                           = "/api/ad/view_ad_message"             // 광고 메세지 조회
public let API_AD_DELETE_AD_MESSAGE                         = "/api/ad/delete_ad_message"           // 광고 메세지 삭제

//거래소 이동
public let API_EXCHANGE_INFO                                = "/api/exchange/info"                  // 거래소 이동 정보 조회
public let API_EXCHANGE_TRANSFER_TOKEN                      = "/api/exchange/transfer_token"        // 토큰 이체 요청

public let URL_ETC_POLICY_USE: String                       = "/terms_of_use"                       // 이용약관
public let URL_ETC_POLICY_PRIVACY: String                   = "/privacy_policy"                     // 개인정보취급방침


// MAKR:- Preference Value
public let PREF_LOGIN_TYPE: String                          = "pref_login_type"
public let PREF_REGIST_TYPE: String                         = "pref_regist_type"
public let PREF_USER_EMAIL: String                          = "pref_user_email"
public let PREF_USER_PHONE: String                          = "pref_user_phone"
public let PREF_USER_NATION_CODE: String                    = "pref_user_nation_code"
public let PREF_USER_PWD: String                            = "pref_user_pwd"
public let PREF_USER_NAME: String                           = "pref_user_name"
public let PREF_USER_PROVIDER_TYPE: String                  = "pref_user_provider_type"
public let PREF_USER_PROVIDER_ID: String                    = "pref_user_provider_id"
public let PREF_USER_PROFILE_IMAGE: String                  = "pref_user_profile_image"
public let PREF_AUTHORIZATION: String                       = "pref_authorization"
public let PREF_TOKEN_KEY: String                           = "pref_token_key"                              //deveice id
public let PREF_LANG: String                                = "pref_lang"
public let PREF_PUSH_TOKEN: String                          = "pref_push_token"
public let PREF_MY_LANG: String                             = "pref_my_lang"                                //내언어 설정
public let PREF_TRAN_LANG: String                           = "pref_tran_lang"                              //번역언어 설정
public let PREF_SET_TRAN_LANG: String                       = "pref_set_tran_lang"                          //번역언어 설정 Y/N
public let PREF_SET_CHAT_BACKGROUND_COLOR: String           = "pref_set_chat_background_color"              //채팅방내 배경색상 Y/N
public let PREF_CHAT_BACKGROUND_COLOR: String               = "pref_chat_background_color"                  //채팅방내 배경색상 값
public let PREF_CHAT_ROOM_BACKGROUND: String                = "pref_chat_room_background"                   //채팅방내 배경이미지
public let PREF_CHAT_NAMES: String                          = "pref_chat_name"                              //채팅방 리스트 이름
public let PREF_SHOP_AUTHORIZATION: String                  = "pref_shop_authorization"                     //쇼핑몰 auth
public let PREF_PERMISSION: String                          = "pref_permission"

public let PREF_VOICE_TRAN_LANGCODE: String                 = "pref_voice_tran_langCode"
public let PREF_VOICE_MY_LANGCODE: String                   = "pref_voice_my_langCode"
public let PREF_CAMERA_TRAN_LANGCODE: String                = "pref_camera_tran_langCode"
public let PREF_CAMERA_MY_LANGCODE: String                  = "pref_camera_my_langCode"
public let PREF_TRANSFER_CAMERA: String                     = "pref_transfer_camera"        //이미지 번역
public let PREF_CONTACTS_NAMES: String                      = "pref_contacts_names"         //연락처
public let PREF_FAVORITE_FRIEND: String                     = "pref_favorite_friend"        //즐겨찾기 친구
public let PREF_NOSHOW_TODAY: String                        = "pref_noshow_today"           //공지사항 오늘하루보지않기

public let NOTI_REFRESH_FRIEND_LIST: String                 = "noti_refresh_freind_list"    //친구리스트 갱신
public let NOTI_REFRESH_CHAT_LIST: String                   = "noti_refresh_chat_list"      //채팅리스트 갱신
public let NOTI_REFRESH_MYPAGE: String                      = "noti_refresh_mypage"         //마이페이지 갱신

public let NOTI_REFRESH_MENU_STATUS: String                 = "noti_refresh_menu_status"     //드라이버 상태 갱신

public let NOTI_DRIVER_FOUNDED: String                    = "noti_drivert_founded"     //드라이버 찾음
public let NOTI_DRIVER_NOT_FOUND: String                    = "noti_driver_not_found"  //드라이버 찾을 수 없음 노티
public let NOTI_DRIVER_REQUEST_COMPLETE: String              = "noti_driver_request_complete"  //운행종료 및 평가 요청

public let APPLE_APP_ID = "1352912092"

public let NOTI_MOVE_MENU_CONTROLLER: String                 = "noti_move_menu_controller"   //메뉴 페이지 이동

// MARK: - Devices
public func isIOS11() -> Bool{
    return CGFloat((UIDevice.current.systemVersion as NSString).floatValue) >= 11.0
}

public func isIOS8() -> Bool{
    return CGFloat((UIDevice.current.systemVersion as NSString).floatValue) >= 8.0
}

public func isIOS7() -> Bool{
    return CGFloat((UIDevice.current.systemVersion as NSString).floatValue) >= 7.0
}

public func isiPhone4() -> Bool{
    return UIScreen.main.bounds.size.height == 480.0
}

public func isiPhone5() -> Bool{
    return UIScreen.main.bounds.size.height == 568.0
}

public func isiPhone8() -> Bool{
    return UIScreen.main.bounds.size.height == 812.0
}

public func isiPhoneX() -> Bool{
    return UIScreen.main.bounds.size.height == 812.0
}

public func isiPhoneXr() -> Bool{
    return UIScreen.main.bounds.size.height == 896.0
}

public func isiPhoneXs() -> Bool{
    return UIScreen.main.bounds.size.height == 896.0
}

public func isiPhoneXsMax() -> Bool{
    return UIScreen.main.bounds.size.height == 896.0
}

public func isiPhoneXseries() -> Bool{
    if isiPhoneX() || isiPhoneXr() || isiPhoneXs() || isiPhoneXsMax() {
        return true
    }
    return false
}

public func getDeviceWidth() -> CGFloat{
    return UIScreen.main.bounds.size.width
}

public func getDeviceHeight() -> CGFloat{
    return UIScreen.main.bounds.size.height
}

public func getTargetWidth() -> CGFloat{
    if(isiPhone4()){
        return 1080
    }
    else{
        return 1080
    }
}

public func getTargetHeight() -> CGFloat{
    if(isiPhone4()){
        return 1620
    }
    else if (isiPhoneXseries()){
        return 2337
    }
    else{
        return 1920
    }
}

public func getDiffHeight() -> CGFloat{
    return 280
}

public func getWidthRatio() -> CGFloat{
    return getDeviceWidth() / getTargetWidth()
}

public func getHeightRatio() -> CGFloat{
    return getDeviceHeight() / getTargetHeight()
}


public func getNumber(_ size : CGFloat) -> CGFloat{
    return (size) * getWidthRatio()
}

public func getSafeAreaInsets() -> UIEdgeInsets {
    if isiPhoneXseries() {
        if #available(iOS 11.0, *) {
            return (UIApplication.shared.keyWindow?.safeAreaInsets)!
        }
    }
    return .zero
}

// MARK: - Color
public func UIColorFromRGBA(_ netHex:Int, alpha:CGFloat) -> UIColor
{
    let red: Int = (netHex >> 16) & 0xff
    let green: Int = (netHex >> 8) & 0xff
    let blue: Int = netHex & 0xff
    
    assert(red >= 0 && red <= 255, "Invalid red component")
    assert(green >= 0 && green <= 255, "Invalid green component")
    assert(blue >= 0 && blue <= 255, "Invalid blue component")
    
    return UIColor(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
}

public func UIColorFromRGBA(_ red: Int, _ green: Int, _ blue: Int, _ alpha: CGFloat) -> UIColor
{
    return UIColor(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
}

//func getDottedLineView(_ rect: CGRect, _ lineColor: UIColor, _ lineWidth: CGFloat) -> UIView {
//    let view = DottedLineView()
//    view.frame = rect
//    view.lineColor = lineColor
//    view.lineWidth = lineWidth
//    return view
//}

// MARK: - Preference
func getPreferenceString(_ preference: String) -> String
{
    let sharedUserDefaults: UserDefaults = UserDefaults.standard
    if let preferen = sharedUserDefaults.value(forKey: preference) as? String{
        return preferen
    }
    return ""
}

func getPreferenceBool(_ preference: String) -> Bool
{
    if let preferen = UserDefaults.standard.value(forKey: preference) as? Bool{
        return preferen
    }
    return false
}

func getPreferenceDouble(_ preference: String) -> Double {
    
    if let preferen = UserDefaults.standard.value(forKey: preference) as? Double{
        return preferen
    }
    return 0.0
}

func getPreferenceFloat(_ preference: String) -> Float {
    
    if let preferen = UserDefaults.standard.value(forKey: preference) as? Float{
        return preferen
    }
    return 0.0
}

func getPreferenceInt(_ preference: String) -> Int {
    
    if let preferen = UserDefaults.standard.value(forKey: preference) as? Int{
        return preferen
    }
    return 0
}

// MARK: - Log
public func println<T>(_ object: T, _ file: String = #file, _ function: String = #function, _ line: Int = #line){
    #if DEBUG
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss:SSS"
    let process = ProcessInfo.processInfo
    
    var tid:UInt64 = 0;
    pthread_threadid_np(nil, &tid);
    let threadId = tid
    
    Swift.print("\(dateFormatter.string(from: NSDate() as Date)) \(process.processName))[\(process.processIdentifier):\(threadId)] \(file.lastPathComponent)(\(line)) \(function):\t\(object)")
    
    //        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    //        let path = documentsPath.stringByAppendingPathComponent("dump.txt")
    //        var dump = ""
    //        if FileManager.default.fileExists(atPath: path) {
    //            dump =  try! String(contentsOfFile: path, encoding: String.Encoding.utf8)
    //        }
    //        do {
    //
    //            dump += "\n\(dateFormatter.string(from: NSDate() as Date)) \(process.processName))[\(process.processIdentifier):\(threadId)] \(file.lastPathComponent)(\(line)) \(function):\t\(object)"
    //            // Write to the file
    //            try  "\(dump)".write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
    //
    //        } catch let error as NSError {
    //            print("Failed writing to log file: \(path), Error: " + error.localizedDescription)
    //        }
    
    #else
    
    #endif
}

public func printlnLog<T>(_ type: Int, _ object: T, _ file: String = #file, _ function: String = #function, _ line: Int = #line){
    #if DEBUG
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss:SSS"
    let process = ProcessInfo.processInfo
    
    var tid:UInt64 = 0;
    pthread_threadid_np(nil, &tid);
    let threadId = tid
    
    switch type {
    case 0:
        Swift.print("\(dateFormatter.string(from: NSDate() as Date)) \(process.processName))[\(process.processIdentifier):\(threadId)] \(file.lastPathComponent)(\(line)) \(function):\t \n=======================paramters=======================\n\(object)\n=======================paramters=======================")
        
        break
    case 1:
        Swift.print("\(dateFormatter.string(from: NSDate() as Date)) \(process.processName))[\(process.processIdentifier):\(threadId)] \(file.lastPathComponent)(\(line)) \(function):\t \n=======================response json=======================\n\(object)\n=======================response json=======================")
        break
    default:
        break
    }
    #else
    
    #endif
}
