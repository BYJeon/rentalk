//
//  HistoryTableViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 05/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AWSS3

class HistoryTableViewController: BaseTableViewController {
    private let PAGE_COUNT = 5
    private let infoManager = InfoManager.getInstance()
    private var resultData: JSON = JSON()
    
    private var searchKeyword = ""
    //채팅방
    private var requestHistory: [RequestInfo] = []
    private var pageCount = 0
    //AWS S3
    private var downloadProgress : Float = 0
    @objc var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
    
    @objc lazy var transferUtility = {
        AWSS3TransferUtility.default()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nibName = UINib(nibName: "HistoryTableViewCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "historyCell")
        
        request(API_DRIVER_RESULT_REQUEST)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return requestHistory.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellInfo = requestHistory[indexPath.row]
        infoManager.requestInfo = cellInfo
        
        if parentDelegate != nil {
            parentDelegate?.moveToPagePresentFromDelegate("RequestStatusDetailViewController")
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as! HistoryTableViewCell
        let cellInfo = requestHistory[indexPath.row]
        cell.carNumLabel.text = cellInfo.car_no
        cell.dataLabel.text = cellInfo.date
        cell.payInfoLabel.text = cellInfo.amt.getCurrencyString() + "원"
        
        if cellInfo.state == "2" { // 탁송 완료
            cell.completeStatusBtn.isHidden = false
            cell.curStatusBtn.isHidden = true
        }else{
            cell.completeStatusBtn.isHidden = true
            cell.curStatusBtn.isHidden = false
        }
        if cellInfo.path_img_s3 != nil {
            cell.pathImage.image = cellInfo.path_img_s3
        }
        
        return cell
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 100.0 {
            print("Padding Last")
            request(API_DRIVER_RESULT_REQUEST)
        }
    }
    
    private func downloadDriverPathInfo(_ imagePath : String, _ reqIndex : Int){
        if imagePath.count == 0 {return}
        let expression = AWSS3TransferUtilityDownloadExpression()
        expression.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                if (self.downloadProgress < Float(progress.fractionCompleted)) {
                    self.downloadProgress = Float(progress.fractionCompleted)
                }
            })
        }
        
        self.completionHandler = { (task, location, data, error) -> Void in
            DispatchQueue.main.async(execute: {
                if let error = error {
                    NSLog("Failed with error: \(error)")
                    //self.statusLabel.text = "Failed"
                }
                else if(self.downloadProgress != 1.0) {
                    //self.statusLabel.text = "Failed"
                }
                else{
                    //self.infoManager.driverInfo!.dirver_image = UIImage(data: data!)
                    
                    let reqInfo = self.requestHistory[reqIndex]
                    reqInfo.path_img_s3 = UIImage(data: data!)
                    self.tableView.reloadData()
                    //print("************************\(reqIndex)*********************")
                }
            })
        }
        
        transferUtility.downloadData(
            forKey: imagePath,
            expression: expression,
            completionHandler: completionHandler).continueWith { (task) -> AnyObject? in
                if let error = task.error {
                    NSLog("Error: %@",error.localizedDescription);
                    DispatchQueue.main.async(execute: {
                        //self.statusLabel.text = "Failed"
                    })
                }
                
                if let _ = task.result {
                    DispatchQueue.main.async(execute: {
                        //self.statusLabel.text = "Downloading..."
                    })
                    NSLog("Download Starting!")
                    // Do something with uploadTask.
                }
                return nil;
        }
    }
    
    open func reloadReuqestInfo(_ keyword: String){
        pageCount = 0
        
        requestHistory.removeAll()
        tableView.reloadData()
        searchKeyword = keyword
        
        request(API_DRIVER_RESULT_REQUEST)
    }
    //MARK: - network
    override func request(_ name: String) {
        switch name {
        case API_DRIVER_RESULT_REQUEST:
            var extraParam : String = "/" + infoManager.getAppID() + "?api_token="+infoManager.getApiToken()+"&count=\(PAGE_COUNT)&start_index=\(pageCount*PAGE_COUNT)"
            if searchKeyword.count > 0 {
                extraParam = extraParam + "&car_no=\(searchKeyword)"
            }
            
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        switch requestID {
        case API_DRIVER_RESULT_REQUEST:
            let itemCnt = requestHistory.count
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                resultData = json["result_array"]
                if resultData.count > 0 {
                    pageCount += 1
                }
                //연락처 이름으로 변경
                for (index, item) in resultData.arrayValue.enumerated() {
                    let imgPath = item["path_img"].stringValue
                    requestHistory.append(RequestInfo(req_id: item["req_id"].stringValue, car_no: item["car_no"].stringValue, car_mdl: item["car_mdl"].stringValue,
                                                      state: item["state"].stringValue, amt: item["amt"].stringValue, date: item["date"].stringValue,
                                                      own_request: item["own_request"].stringValue, path_img: imgPath))
                    
                    
                    DispatchQueue.main.async(execute: {
                        if imgPath.count > 1 {
                            let pre = imgPath.index(imgPath.startIndex, offsetBy: 0)
                            let prefix = String(imgPath[pre])
                            
                            if prefix == "r"{
                                self.downloadDriverPathInfo(imgPath, itemCnt + index)
                            }else{
                                let startIndex = imgPath.index(imgPath.startIndex, offsetBy: 1)
                                self.downloadDriverPathInfo(String(imgPath[startIndex...]), itemCnt + index)
                            }
                            
                        }
                    })
                }
                
                tableView.reloadData()
            }
            break
        
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        let dialog = UIAlertController(title: "오류", message: "관리자에게 문의하세요.", preferredStyle: .alert)
        let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
        dialog.addAction(action)
        self.present(dialog, animated: true, completion: nil)
    }
}
