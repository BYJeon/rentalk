//
//  MainViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 07/08/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import NMapsMap
import Alamofire
import SwiftyJSON
import AWSS3
import Mixpanel

@objc protocol MainOperationDelegate{
    func setRequestPathToMap()
    func moveToPage(id: String)
    func moveToPageByPresent(id: String)
    func popToRootViewConterller()
    @objc optional func moveToPageByPresent(id: String, title: String, url: String)
    @objc optional func moveToPageByPresent(id: String, reqID: String)
}

class MainViewController: BaseMapViewController, MainOperationDelegate {

    @IBOutlet var topGradientView: UIView!
    
    private let pathInfoManager : PathInfoManager = PathInfoManager.sharedInstance
    private let infoManager = InfoManager.getInstance()
    private let targetMarker = NMFMarker()
    private let sourceMarker = NMFMarker()
    private let viaMarker = NMFMarker()
    private let locationManager = CLLocationManager()
    
    @IBOutlet var driverRequestView: UIView!
    @IBOutlet var pathInfoView: UIView!
    // 드라이버 선정 View
    @IBOutlet var driverInfoView: UIView!
    @IBOutlet var driverNameLabel: UILabel!
    @IBOutlet var driverPayLabel: UILabel!
    
    @IBOutlet var remainTimeLabel: UILabel!
    @IBOutlet var startByCompanyView: UIView!
    @IBOutlet var startByCustomerView: UIView!
    
    @IBOutlet var companyCheckBtn: CheckBox!
    @IBOutlet var customerCheckBtn: CheckBox!
    @IBOutlet var driverReqeustBtn: RoundButton!
    
    @IBOutlet var driverImage: UIImageView!
    @IBOutlet var driverGapLabel: UILabel!
    @IBOutlet var driverAmountLabel: UILabel!
    @IBOutlet var driverRemainTimeLabel: UILabel!
    
    private var driverConfirmStatus = "3"
    //timer
    private var mTimer : Timer?
    private var remainTime = 29
    
    @IBOutlet var screenShot: UIImageView!
    @IBOutlet var driverRequestLabel: UILabel!
    
    private var uploadProgress : Float = 0
    
    private let startWorkingTime = 9
    private let endWorkingTime = 18
    
    @objc var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    @objc var progressBlock: AWSS3TransferUtilityProgressBlock?
    @objc lazy var transferUtility = {
        AWSS3TransferUtility.default()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenShot.isHidden = true
        //메뉴 이동 noti
        NotificationCenter.default.addObserver(self, selector: #selector(self.moveToPageBySideMenu(_:)), name: NSNotification.Name(rawValue: NOTI_MOVE_MENU_CONTROLLER), object: nil)
        //운행 종료 및
        NotificationCenter.default.addObserver(self, selector: #selector(self.moveToDriverScorePage(_:)), name: NSNotification.Name(rawValue: NOTI_DRIVER_REQUEST_COMPLETE), object: nil)
        
        mapView?.isNightModeEnabled = true
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization() //권한 요청
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        if isWorkingTime() {
            let gestureRequest = UITapGestureRecognizer(target: self, action: #selector(self.driverRequest(_:)))
            self.driverRequestLabel.addGestureRecognizer(gestureRequest)
        }else{
            self.driverRequestLabel.text = "운영시간이 아닙니다."
            #if DEBUG
            self.driverRequestLabel.text = "디버깅 모드"
            let gestureRequest = UITapGestureRecognizer(target: self, action: #selector(self.driverRequest(_:)))
            self.driverRequestLabel.addGestureRecognizer(gestureRequest)
            #endif
        }
        
        let gestureCompany = UITapGestureRecognizer(target: self, action: #selector(self.startByCompany(_:)))
        self.startByCompanyView.addGestureRecognizer(gestureCompany)
        
        let gestureCustomer = UITapGestureRecognizer(target: self, action: #selector(self.startByCustomer(_:)))
        self.startByCustomerView.addGestureRecognizer(gestureCustomer)
        
        //S3 파일 업로드
        self.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                if (self.uploadProgress < Float(progress.fractionCompleted)) {
                    self.uploadProgress = Float(progress.fractionCompleted)
                }
            })
        }
        
        self.completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                self.showLoadingProgressBar(isShow: false)
                if let error = error {
                    print("Failed with error: \(error)")
                    //self.statusLabel.text = "Failed"
                }
                else if(self.uploadProgress != 1.0) {
                    //self.statusLabel.text = "Failed"
                    NSLog("Error: Failed - Likely due to invalid region / filename")
                }
                else{
                    if self.pathInfoManager.isCompanyStart() {
                        if self.infoManager.companyTypeGb == "1"{
                           self.moveToPageByPresent(id: "CompanyStartViewController")
                        }else{
                           self.moveToPageByPresent(id: "CompanyStartNormalViewController")
                        }
                    }else {
                        if self.infoManager.companyTypeGb == "1"{
                            self.moveToPageByPresent(id: "CustomerStartViewController")
                        }else{
                            self.moveToPageByPresent(id: "CustomerStartNormalViewController")
                        }
                    }
                }
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        topGradientView.bottomAlphGradient(startPos: 0.0, endPos: 0.75, viewWidth: self.view.frame.width, alph: 0.8)
        setBottomViewStatus()
        
        request(API_APP_STATUS_REQUEST)
    }
    
    func isWorkingTime() -> Bool {
        
        let cal = Calendar(identifier: .gregorian)
        let now = Date()
        let coms = cal.dateComponents([.weekday], from: now)
        //일요일 1 - 토요일 7
        if coms.weekday == 1 || coms.weekday == 7 {return false}
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH"
        dateFormatter.locale = NSLocale(localeIdentifier: "ko_KR") as Locale
        let curTime = Int(dateFormatter.string(from: now))
        
        #if DEBUG
        return true
        #endif
        
        if curTime! < startWorkingTime || endWorkingTime < curTime! { return false }
        return true
    }
    //드라이버 정보 View Event
    @IBAction func driverConfirmDidClicked(_ sender: Any) {
        print("Driver Confirm")
        driverConfirmStatus = "1"
        request(API_DRIVER_INFO_REQUEST)
    }
    
    @IBAction func driverCancelDidClicked(_ sender: Any) {
        #if RELEASE
        Mixpanel.mainInstance().track(event: "PURCHASE_REFUND",
                                      properties:["CURRENCY": "KRW", "VALUE": infoManager.driverInfo!.expected_amt, "TRANSACTION_ID": infoManager.reqID])
        #endif
        
        print("Driver Cancel")
        driverConfirmStatus = "2"
        request(API_DRIVER_INFO_REQUEST)
    }
    
    
    // MARK: - Navigation
    @IBAction func menuDidClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    @IBAction func requestDidClicked(_ sender: Any) {
        
        if isWorkingTime() {
            moveToPage(id: "PathInfoViewController")
        }
    }
    
    //드라이버 요청 확인
    @IBAction func driverRequestDidClicked(_ sender: Any) {
        
        #if RELEASE
        Mixpanel.mainInstance().track(event: "SET_CHECKOUT_OPTION",
                                      properties:["CHECKOUT_STEP": "3", "CHECKOUT_OPTION": "DETAILS"])
        
        #endif
        showLoadingProgressBar(isShow: true)
        
        let now=NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        
        let img = mapView?.takeScreenshot(pathInfoView.bounds.height)
        let imgPath = "rentalk/" + infoManager.getAppID() + dateFormatter.string(from: now as Date)+".png"
        infoManager.driverImgFilePath = imgPath
        uploadImage(with: (img?.pngData()!)!, keyName: imgPath)
        //screenShot.image = img
        //screenShot.isHidden = false

    }
    
    @objc func moveToPageBySideMenu(_ notification: NSNotification) {
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["id"] as? String{
                infoManager.setMenuType(type: .NORMAL)
                
                if id == "CustomerViewController"{
                    moveToPageByPresent(id: id)
                }else{
                    if id == "PathInfoViewController"{
                        #if RELEASE
                        if !isWorkingTime() {return}
                        #endif
                    }
                    moveToPage(id: id)
                }
                
//                let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseSwipeViewController
//                pathVC.mainOperationDelegate = self
//                self.navigationController?.pushViewController(pathVC, animated: true)
            }
        }
    }
    
    @objc func moveToDriverScorePage(_ notification: NSNotification) {
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DriverScoreViewController") as! BaseSwipeViewController
        pathVC.mainOperationDelegate = self
        self.present(pathVC, animated: true, completion: nil)
    }
    
    //S3 이미지 업로드
    func uploadImage(with data: Data, keyName: String) {
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = progressBlock
        expression.setValue("public-read", forRequestHeader: "x-amz-acl")
        
        DispatchQueue.main.async(execute: {
            self.uploadProgress = 0
        })
        
        transferUtility.uploadData(
            data,
            key: keyName,
            contentType: "image/png",
            expression: expression,
            completionHandler: completionHandler).continueWith { (task) -> AnyObject? in
                if let error = task.error {
                    print("Error: \(error.localizedDescription)")
                    
                    DispatchQueue.main.async {
                        //self.statusLabel.text = "Failed"
                    }
                }
                
                if let _ = task.result {
                    
                    DispatchQueue.main.async {
                        //self.statusLabel.text = "Uploading..."
                        print("Upload Starting!")
                    }
                    
                    // Do something with uploadTask.
                }
                
                return nil;
        }
    }
    
    // MARK : MainOperationDelegate
    // 어디에서 출발할까요?
    @objc func driverRequest(_ sender: UIGestureRecognizer){
        moveToPage(id: "PathInfoViewController")
    }
    
    // 회사에서 출발 탁송 선택 Gesture
    @objc func startByCompany(_ sender: UIGestureRecognizer){
        companyCheckBtn.isChecked = true
        customerCheckBtn.isChecked = false
        
        startByCompanyView.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 0.3)
        startByCustomerView.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 0.0)
        driverReqeustBtn.isHidden = false
        
        pathInfoManager.setCompanyStart(true)
    }
    
    // 고객에서 출발 탁송 선택 Gesture
    @objc func startByCustomer(_ sender: UIGestureRecognizer){
        companyCheckBtn.isChecked = false
        customerCheckBtn.isChecked = true
        
        startByCompanyView.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 0.0)
        startByCustomerView.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 0.3)
        driverReqeustBtn.isHidden = false
        
        pathInfoManager.setCompanyStart(false)
    }
    
    func setBottomViewStatus(){
        
        let locationOverlay = mapView?.locationOverlay
        locationOverlay?.hidden = true
        
        // 드라이버가 선택 되어 메인으로 넘어 왔을 경우
        if infoManager.getMenuType() == .DRIVERPICK {
            #if RELEASE
            Mixpanel.mainInstance().track(event: "ECOMMERCE_PURCHASE",
                                          properties:["CURRENCY": "KRW", "VALUE": infoManager.driverInfo!.expected_amt, "TRANSACTION_ID": infoManager.reqID])
            #endif
            remainTime = 29
            
            driverImage.image = infoManager.driverInfo?.dirver_image
            driverNameLabel.text = infoManager.driverInfo?.driver_nm
            driverGapLabel.text = String.init(format: "%.2f km", Double(infoManager.driverInfo!.dist_gap)!/1000)
            driverAmountLabel.text = infoManager.driverInfo!.expected_amt.getCurrencyString() + "원"
            
            pathInfoView.isHidden = true
            driverRequestView.isHidden = true
            driverInfoView.isHidden = false
            // 탁송목적 선택하기 View 초기화
            companyCheckBtn.isChecked = false
            customerCheckBtn.isChecked = false
            
            driverReqeustBtn.isHidden = true
            startByCustomerView.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 0.0)
            startByCompanyView.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 0.0)
            
            if let timer = mTimer {
                //timer 객체가 nil 이 아닌경우에는 invalid 상태에만 시작한다
                if !timer.isValid {
                    /** 1초마다 timerCallback함수를 호출하는 타이머 */
                    mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
                }
            }else{
                //timer 객체가 nil 인 경우에 객체를 생성하고 타이머를 시작한다
                /** 1초마다 timerCallback함수를 호출하는 타이머 */
                mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
            }
            
        }else if infoManager.getMenuType() == .NORMAL{ //탁송 요청 상태
            driverConfirmStatus = "3"
            pathInfoView.isHidden = true
            driverRequestView.isHidden = false
            driverInfoView.isHidden = true
            
            pathInfoManager.setPathInfo(item: nil, pathType: .TARGET, isViaMode: false)
            pathInfoManager.setPathInfo(item: nil, pathType: .SOURCE, isViaMode: false)
            pathInfoManager.setPathInfo(item: nil, pathType: .REPAIR_SHOP, isViaMode: false)
            
            setRequestPathToMap()
            
            setMyLocationPos()
        }else if infoManager.getMenuType() == .SET_REQUEST_PATH{ //목적지를 설정 했을 경우
            pathInfoView.isHidden = false
            driverRequestView.isHidden = true
            driverInfoView.isHidden = true
            
            // 탁송목적 선택하기 View 초기화
            companyCheckBtn.isChecked = false
            customerCheckBtn.isChecked = false
            
            driverReqeustBtn.isHidden = true
            startByCustomerView.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 0.0)
            startByCompanyView.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 0.0)
        }
    }
    
    //나의 위치 표시
    func setMyLocationPos(){
        if pathInfoManager.myLocationInfo == nil { return }
        let latitude = pathInfoManager.myLocationInfo!.coordinate.latitude
        let longitude = pathInfoManager.myLocationInfo!.coordinate.longitude
        
        let cameraUpdate = NMFCameraUpdate(scrollTo: NMGLatLng(lat: latitude, lng: longitude))
        mapView!.moveCamera(cameraUpdate)
        
        let locationOverlay = mapView?.locationOverlay
        locationOverlay?.hidden = false
        locationOverlay?.location = NMGLatLng(lat: latitude, lng: longitude)
    }
    
    //타이머가 호출하는 콜백함수
    @objc func timerCallback(){
        if remainTime > 0 {
            remainTime -= 1
            driverRemainTimeLabel.text = String(format: "%d초 후 자동으로 확정됩니다.", remainTime)
        }else if remainTime == 0 {
            driverRemainTimeLabel.text = String(format: "%d초 후 자동으로 확정됩니다.", remainTime)
            driverConfirmStatus = "1"
            request(API_DRIVER_INFO_REQUEST)
            mTimer?.invalidate()
        }
        
    }
    
    // 지도에 출발/도착/경유지 표시
    func setRequestPathToMap() {
        targetMarker.mapView = nil
        sourceMarker.mapView = nil
        viaMarker.mapView = nil
        
        let targetPath = pathInfoManager.getTargetInfo()
        let sourcePath = pathInfoManager.getSourceInfo()
        let viaPath = pathInfoManager.getRepariShopInfo()
        
        pathInfoView.isHidden = true
        driverRequestView.isHidden = false
        
        // 탁송목적 선택하기 View 초기화
        companyCheckBtn.isChecked = false
        customerCheckBtn.isChecked = false
        
        driverReqeustBtn.isHidden = true
        startByCustomerView.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 0.0)
        startByCompanyView.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 0.0)
        
        if targetPath == nil || sourcePath == nil {return}
        
        pathInfoView.isHidden = false
        driverRequestView.isHidden = true
        
        if targetPath != nil {
            setMarkerInMap(marker: targetMarker, lat: targetPath!.getLatInfo(), lng: targetPath!.getLngInfo(), title: "출발")
        }
        
        if sourcePath != nil {
            setMarkerInMap(marker: sourceMarker, lat: sourcePath!.getLatInfo(), lng: sourcePath!.getLngInfo(), title: "도착")
        }
        
        if viaPath != nil{
            setMarkerInMap(marker: viaMarker, lat: viaPath!.getLatInfo(), lng: viaPath!.getLngInfo(), title: "경유")
        }
        
        let southWestPos = pathInfoManager.getSouthWestPathInfo()
        let northEastPos = pathInfoManager.getNorthEastPathInfo()
        
        if southWestPos != nil && northEastPos != nil {
            let bounds = NMGLatLngBounds(southWestLat: southWestPos!.getLatInfo(), southWestLng: southWestPos!.getLngInfo(), northEastLat: northEastPos!.getLatInfo(), northEastLng: northEastPos!.getLngInfo())
            
            let edge = UIEdgeInsets.init(top: getNumber(750), left: getNumber(200), bottom: pathInfoView.bounds.height + getNumber(400), right: getNumber(200))
            let cameraUpdate = NMFCameraUpdate(fit: bounds, paddingInsets: edge)
            
            cameraUpdate.animation = .fly
            cameraUpdate.animationDuration = 1
            mapView!.moveCamera(cameraUpdate)
            mapView!.sizeToFit()
        }
    }
    
    //root view를 최상단으로 올리자
    func popToRootViewConterller() {
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: false, completion: nil)
    }
    
    func moveToPage(id: String) {
        infoManager.setMenuType(type: .NORMAL)
        
        if id == "PathInfoViewController"{
            #if RELEASE
            Mixpanel.mainInstance().track(event: "SET_CHECKOUT_OPTION",
                                          properties:["CHECKOUT_STEP": "1", "CHECKOUT_OPTION": "ADDRESS"])
            #endif
        }
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseSwipeViewController
        pathVC.mainOperationDelegate = self
        self.navigationController?.pushViewController(pathVC, animated: true)
    }
    
    func moveToPagePresentFromDelegate(_ id: String, reqID: String) {
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseMapViewController
        pathVC.driverRedID = reqID
        getTopMostViewController()?.present(pathVC, animated: true, completion: nil)
    }
    
    func moveToPageByPresent(id: String, title: String, url: String) {
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseSwipeViewController
        pathVC.titleInfo = title
        pathVC.urlInfo = url
        pathVC.mainOperationDelegate = self
        getTopMostViewController()?.present(pathVC, animated: true, completion: nil)
    }
    
    func moveToPageByPresent(id: String) {
        infoManager.setMenuType(type: .NORMAL)
        
        let pathVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as! BaseSwipeViewController
        pathVC.mainOperationDelegate = self
        getTopMostViewController()?.present(pathVC, animated: true, completion: nil)
    }
    
    func getTopMostViewController() -> UIViewController? {
        var topMostViewController = UIApplication.shared.keyWindow?.rootViewController
        
        while let presentedViewController = topMostViewController?.presentedViewController {
            topMostViewController = presentedViewController
        }
        
        return topMostViewController
    }
    
    func setMarkerInMap(marker: NMFMarker, lat: Double, lng: Double, title: String){
        marker.position = NMGLatLng(lat: lat, lng: lng)
        let markerImg = title == "출발" ? "ico_map_pincircle_g" : "ico_map_pincircle_y"
        marker.iconImage = NMFOverlayImage(name: markerImg)
        marker.mapView = mapView
        
        let infoWindow = NMFInfoWindow()
        let customInfo = CustomInfoWindow(title: title)
        infoWindow.dataSource = customInfo
        infoWindow.open(with: marker)
    }
    
    //MARK: - network
    override func request(_ name: String) {
        switch name {
        case API_DRIVER_INFO_REQUEST:
            let req_id : String = infoManager.reqID
            let driver_id : String = infoManager.driverInfo!.driver_id
            let token : String  = infoManager.getApiToken()
            
            let params: Parameters = [
                "req_id" : req_id
                ,"confirm_state" : driverConfirmStatus
                ,"driver_id" : driver_id
                ,"api_token" : token
            ]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.put, target: self)
            break
        case API_DRIVER_REQUEST_CANCLE:
            let params: Parameters = [
                "req_id" : infoManager.reqID
                ,"api_token" : infoManager.getApiToken()
            ]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.put, target: self)
            break
            
        case API_APP_STATUS_REQUEST:
            let extraParam : String = "/" + infoManager.getAppID() + "?api_token="+infoManager.getApiToken() + "&fcm_token="+infoManager.getToken()
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        switch requestID {
        case API_DRIVER_INFO_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                mTimer?.invalidate()
                if driverConfirmStatus == "1" { //드라이버 요청 수락
                    infoManager.setMenuType(type: .NORMAL)
                }else if driverConfirmStatus == "2" { //드라이버 요청 거절
                    infoManager.setMenuType(type: .NORMAL)
                }else{ //드라이버 요청 시간 만료
                    infoManager.setMenuType(type: .NORMAL)
                }
                
                if driverConfirmStatus == "1"{ //드라이버 요청 수락일 경우
                    //푸쉬로 받은 정보 입력
                    let reqInfo = RequestInfo(req_id: infoManager.reqID, state: driverConfirmStatus)
                    infoManager.requestInfo = reqInfo
                    moveToPage(id: "RequestStatusDetailViewController")
                }
                
                setBottomViewStatus()
            }
            break;
        case API_DRIVER_REQUEST_CANCLE:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
            }
            dismiss(animated: true, completion: nil)
            break
        case API_APP_STATUS_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                
            }else{
                moveToPageByPresent(id: "LoginViewController")
            }
            break
        default:
            break
        }
    }
    
    func pressed()
    {
        moveToPageByPresent(id: "LoginViewController")
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        //let json = JSON(data!)
        
        let dialog = UIAlertController(title: "오류", message: "관리자에게 문의하세요.", preferredStyle: .alert)
        let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default, handler: {action in self.pressed()})
        dialog.addAction(action)
        self.present(dialog, animated: true, completion: nil)
    }
}

extension MainViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let firstLocation = locations.first else {
            return
        }
        
        //print("안녕 : \(firstLocation.coordinate.latitude)")
        
        if pathInfoManager.myLocationInfo == nil {
            pathInfoManager.myLocationInfo = firstLocation
            setMyLocationPos()
        }
    }
}

extension UIView{
    //하단 방향 Alph Gradient
    func bottomAlphGradient(startPos: Double, endPos: Double , viewWidth: CGFloat, alph: Double){
        let gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0.5, y: startPos)
        gradient.endPoint = CGPoint(x: 0.5, y: endPos)
        let whiteColor = UIColor.white
        gradient.colors = [whiteColor.withAlphaComponent(CGFloat(alph)).cgColor, whiteColor.withAlphaComponent(CGFloat(alph)).cgColor, whiteColor.withAlphaComponent(0.0).cgColor]
        gradient.locations = [NSNumber(value: 0.0),NSNumber(value:endPos),NSNumber(value: 1.0)]
        gradient.frame = CGRect(x: 0, y: 0, width: viewWidth, height: self.bounds.height)
        self.layer.mask = gradient
    }
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}
