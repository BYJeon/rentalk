//
//  BaseSwipeViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 07/08/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit

@objc protocol ParentViewDelegate{
    func showPopupViewFromDelegate(_ title: String, _ content: String)
    func moveToPagePresentFromDelegate(_ id: String)
    @objc optional func moveToPagePresentFromDelegate(_ id: String, title: String, urlInfo: String)
    @objc optional func moveToPagePresentFromDelegate(_ id: String, reqID: String)
    @objc optional func childViewRequestComplete()
    @objc optional func childViewRequestToServer()
    @objc optional func takeScreenShot()
}

class BaseSwipeViewController: UIViewController, NetManagerDelegate, ParentViewDelegate {
    
    func moveToPagePresentFromDelegate(_ id: String, title: String, urlInfo: String) {
    }
    func showPopupViewFromDelegate(_ title: String, _ content: String) {
    }
    
    func moveToPagePresentFromDelegate(_ id: String) {
    }
    
    func childViewRequestComplete() {
        showLoadingProgressBar(isShow: false)
    }
    
    func childViewRequestToServer() {
        showLoadingProgressBar(isShow: true)
    }
    
    internal var appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
    weak var mainOperationDelegate : MainOperationDelegate? = nil
    
    internal var titleInfo = ""
    internal var urlInfo = ""
    
    internal var loadingBgBView : UIView? = nil
    internal var loadingImage : UIImageView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        
        //로딩 화면
        loadingBgBView = UIView()
        loadingBgBView!.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height) // Set X and Y whatever you want
        loadingBgBView!.backgroundColor = UIColor.colorWithRGBHex(hex: 0x000000, alpha: 0.7)
        loadingBgBView!.isHidden = true
        
        loadingImage = UIImageView()
        loadingImage!.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        loadingImage!.image = UIImage(named: "loading_01")
        loadingImage!.center = (loadingBgBView?.convert(loadingBgBView!.center, from:loadingBgBView?.superview))!
        
        let animationsFrames = [UIImage(named: "loading_01"), UIImage(named: "loading_02"),
                               UIImage(named: "loading_03"), UIImage(named: "loading_04")]
        loadingImage?.animationImages = (animationsFrames as! [UIImage])
        loadingImage?.animationDuration = 0.8
        //loadingImage?.startAnimating()
        loadingBgBView?.addSubview(loadingImage!)
        
        self.view.addSubview(loadingBgBView!)
    }
    

    //MARK: - NetManager delegate
    func request(_ name: String) {
        println(">>> requestID = \(name)")
    }
    
    func requestFinished(_ requestID: String, _ data: Any!) {
    }
    
    func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
    }
    
    func requestFailed(_ requestID: String) {
        println(">>> requestID = \(requestID)")
    }
    
    func showLoadingProgressBar(isShow: Bool){
        if isShow {
            loadingBgBView?.isHidden = false
            loadingImage?.startAnimating()
        }else{
            loadingBgBView?.isHidden = true
            loadingImage?.startAnimating()
        }
    }
}
