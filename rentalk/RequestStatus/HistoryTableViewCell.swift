//
//  HistoryTableViewCell.swift
//  rentalk
//
//  Created by Yuna Daddy on 05/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet var dataLabel: UILabel!
    @IBOutlet var carNumLabel: UILabel!
    @IBOutlet var payInfoLabel: UILabel!
    @IBOutlet var curStatusBtn: RoundButton!
    @IBOutlet var completeStatusBtn: RoundButton!
    
    @IBOutlet var pathImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
