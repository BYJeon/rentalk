//
//  SideMenuDetailController.swift
//  rentalk
//
//  Created by Yuna Daddy on 07/08/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit

class SideMenuDetailController: UITableViewController {

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        var id : String = ""
        
        if(indexPath.row == 3){ // 탁송신청
            id = "PathInfoViewController"
        }else if(indexPath.row == 4){ // 탁송이력
            id = "historyViewController"
        }else if(indexPath.row == 5){ // 고객센터
            id = "CustomerViewController"
        }else if(indexPath.row == 6){ // 더보기
            id = "MoreViewController"
        }
        
        let menuIdx:[String: String] = ["id": id]
        
        if( row > 2 && id.count > 0){
            //sideMenuController?.setContentViewController(with: "\(row)", animated: Preferences.shared.enableTransitionAnimation)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI_MOVE_MENU_CONTROLLER), object: nil, userInfo: menuIdx)
        }
        
        sideMenuController?.hideMenu()
    }
    override func viewDidLoad() {
        //Cell 선택 하이라이트 제거
        let clearView = UIView()
        clearView.backgroundColor = UIColor.clear // Whatever color you like
        UITableViewCell.appearance().selectedBackgroundView = clearView
    }
    
    // 메뉴 닫기 클릭
    @IBAction func navCloseDidClicked(_ sender: Any) {
        sideMenuController?.hideMenu()
    }

}
