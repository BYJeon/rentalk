//
//  NetworkManager.swift
//  rentalk
//
//  Created by Yuna Daddy on 16/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import Alamofire
import SwiftyJSON
import UIKit

@objc protocol NetManagerDelegate {
    @objc optional func downloadProgress(completedValue: Double)
    @objc optional func request(_ name: String)
    @objc optional func requestFinished(_ requestID: String, _ data: Any!)
    @objc optional func requestFailed(_ requestID: String)
    @objc optional func requestResultFail(_ requestID: String, _ data: Any!)
}

class NetworkManager {
    
    public let ATTR_CONTENT_TYPE: String = "Content-Type"
    public let ATTR_ACCEPT: String = "accept"
    
    public let CONTENT_TYPE: String = "application/json;charset=UTF-8"
    public let CONTENT_TYPE_NOUTF: String = "application/json"
    public let AUTHORIZATION: String = "Authorization"
    public let LOGIN_TYPE: String = "X-Login-Type"                                     //A : App, F : Facebook, G : Google+
    public let LANG: String = "X-Lang"
    public let APP_OS = "OS"
    public let APP_OS_VERSION = "OSVersion"
    public let APP_MODEL = "Model"
    
    static let sharedInstance: NetworkManager = {
        let instance = NetworkManager()
        return instance
    }()
    
    func getDriverInfoHeader() -> HTTPHeaders {
        let headers = [
            ATTR_CONTENT_TYPE : CONTENT_TYPE_NOUTF
            , ATTR_ACCEPT : CONTENT_TYPE_NOUTF
        ]
        return headers
    }
    
    func getDefaulAuthHeader() -> HTTPHeaders {
        let headers = [
            ATTR_CONTENT_TYPE : CONTENT_TYPE
            , LANG : getPreferenceString(PREF_LANG)
            , APP_OS : "I"
            , APP_OS_VERSION : UIDevice.current.systemVersion
            , APP_MODEL : UIDevice.current.model
        ]
        return headers
    }
    
    func getAuthHeader() -> HTTPHeaders {
        let headers = [
            ATTR_CONTENT_TYPE : CONTENT_TYPE
            , AUTHORIZATION : getPreferenceString(PREF_AUTHORIZATION)
            , LANG : getPreferenceString(PREF_LANG)
            , LOGIN_TYPE : getPreferenceString(PREF_LOGIN_TYPE)
            , APP_OS : "ios"
            , APP_OS_VERSION : UIDevice.current.systemVersion
            , APP_MODEL : UIDevice.current.model
        ]
        return headers
    }
    
    func request(_ requestName: String, _ param: Dictionary<String, Any>, _ reqType: HTTPMethod, target: NetManagerDelegate!) -> DataRequest?{
        
        let serverUrl: String = API_SERVER
        let url = serverUrl + requestName
        let params: Dictionary<String, Any> = param
        let methods: HTTPMethod = reqType
        
        return Alamofire.request(url, method: methods, parameters: params, encoding: JSONEncoding.default, headers: getDefaulAuthHeader()).responseJSON(completionHandler:  {
            response in
            
            switch response.result {
            case .success:
                self.printRequestInfo(url, methods.rawValue, params.description, response.result, response.result.value)
                if let value = response.result.value {
                    
                    let json = JSON(value)
                    if json["success"].stringValue == "0000" {
                        target.requestFinished!(requestName, value)
                    } else {
                        target.requestResultFail!(requestName, value)
                    }
                }
                
            case .failure(let error):
                self.printRequestInfo(url, methods.rawValue, params.description, response.result, error)
                target.requestFailed!(requestName)
            }
        })
    }
    
    func requestSlack(_ requestName: String, _ param: Dictionary<String, Any>, _ reqType: HTTPMethod, target: NetManagerDelegate!) -> DataRequest?{
        
        let url = requestName
        let params: Dictionary<String, Any> = param
        let methods: HTTPMethod = reqType
        
        return Alamofire.request(url, method: methods, parameters: params, encoding: JSONEncoding.default, headers: getDriverInfoHeader()).responseJSON(completionHandler:  {
            response in
            
            if let result = String(bytes: response.data!, encoding: .utf8) {
                if result == "ok"{
                    self.printRequestInfo(url, methods.rawValue, params.description, response.result, response.result.value)
                    target.requestFinished!(requestName, response.result.value)
                }
            }else{
                self.printRequestInfo(url, methods.rawValue, params.description, response.result, 0)
                target.requestFailed!(requestName)
            }
            
        })
    }
    
    func requestGet(_ requestName: String, _ param: String, _ reqType: HTTPMethod, target: NetManagerDelegate!) -> DataRequest?{
        
        let serverUrl: String = API_SERVER
        let url = serverUrl + requestName + param
        let methods: HTTPMethod = reqType
        
        return Alamofire.request(url, method: methods, parameters: nil, encoding: JSONEncoding.default, headers: getDriverInfoHeader()).responseJSON(completionHandler:  {
            response in
            
            switch response.result {
            case .success:
                self.printRequestInfo(url, methods.rawValue, url, response.result, response.result.value)
                if let value = response.result.value {
                    
                    let json = JSON(value)
                    if json["success"].stringValue == "0000" {
                        target.requestFinished!(requestName, value)
                    } else {
                        target.requestResultFail!(requestName, value)
                    }
                }
                
            case .failure(let error):
                self.printRequestInfo(url, methods.rawValue, url, response.result, error)
                target.requestFailed!(requestName)
            }
        })
    }
    
    func requestFormdata(_ requestName: String, _ param: Dictionary<String, Any>, target: NetManagerDelegate!) -> DataRequest?{
        
        let serverUrl: String = API_SERVER
        let url = serverUrl + requestName
        let params: Dictionary<String, Any> = param
        let methods: HTTPMethod = .post
        
        return Alamofire.request(url, method: methods, parameters: params, encoding: JSONEncoding.default, headers: getAuthHeader()).responseJSON(completionHandler:  {
            response in
            
            switch response.result {
            case .success:
                self.printRequestInfo(url, methods.rawValue, param.description, response.result, response.result.value)
                if let value = response.result.value {
                    
                    let json = JSON(value)
                    if json["code"].stringValue == "0000" {
                        target.requestFinished!(requestName, value)
                    } else {
                        target.requestResultFail!(requestName, value)
                    }
                }
                
            case .failure(let error):
                self.printRequestInfo(url, methods.rawValue, param.description, response.result, error)
                target.requestFailed!(requestName)
            }
        })
    }
    
    func requestFormdataDebug(_ requestName: String, _ param: Dictionary<String, Any>, target: NetManagerDelegate!) -> DataRequest?{
        
        let serverUrl: String = API_SERVER
        let url = serverUrl + requestName
        let params: Dictionary<String, Any> = param
        let methods: HTTPMethod = .post
        
        return Alamofire.request(url, method: methods, parameters: params, encoding: JSONEncoding.default, headers: getAuthHeader()).responseString(completionHandler:  {
            response in
            
            switch response.result {
            case .success:
                self.printRequestInfo(url, methods.rawValue, param.description, response.result, response.result.value)
                if let value = response.result.value {
                    
                    let json = JSON(value)
                    if json["code"].stringValue == "0000" {
                        target.requestFinished!(requestName, value)
                    } else {
                        target.requestResultFail!(requestName, value)
                    }
                }
                
            case .failure(let error):
                self.printRequestInfo(url, methods.rawValue, param.description, response.result, error)
                target.requestFailed!(requestName)
            }
        })
    }
    
    func requestBody(_ requestName: String, _ param: String, target: NetManagerDelegate!) -> DataRequest?{
        
        let serverUrl: String = API_SERVER
        let url = serverUrl + requestName
        
        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue(CONTENT_TYPE, forHTTPHeaderField: ATTR_CONTENT_TYPE)
        request.addValue(getPreferenceString(PREF_AUTHORIZATION), forHTTPHeaderField: AUTHORIZATION)
        request.addValue(getPreferenceString(PREF_LANG), forHTTPHeaderField: LANG)
        request.addValue(getPreferenceString(PREF_LOGIN_TYPE), forHTTPHeaderField: LOGIN_TYPE)
        request.addValue("ios", forHTTPHeaderField: APP_OS)
        request.addValue(UIDevice.current.systemVersion, forHTTPHeaderField: APP_OS_VERSION)
        request.addValue(UIDevice.current.model, forHTTPHeaderField: APP_MODEL)
        request.httpBody = param.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        return Alamofire.request(request).responseJSON(completionHandler:  {
            response in
            
            switch response.result {
            case .success:
                self.printRequestInfo(url, (request.httpMethod?.description)!, param.description, response.result, response.result.value)
                if let value = response.result.value {
                    
                    let json = JSON(value)
                    if json["code"].stringValue == "0000" {
                        target.requestFinished!(requestName, value)
                    } else {
                        target.requestResultFail!(requestName, value)
                    }
                }
                
            case .failure(let error):
                self.printRequestInfo(url, (request.httpMethod?.description)!, param.description, response.result, error)
                target.requestFailed!(requestName)
            }
        })
    }
    
    func requestBodyDebug(_ requestName: String, _ param: String, target: NetManagerDelegate!) -> DataRequest?{
        
        let serverUrl: String = API_SERVER
        let url = serverUrl + requestName
        
        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue(CONTENT_TYPE, forHTTPHeaderField: ATTR_CONTENT_TYPE)
        request.addValue(getPreferenceString(PREF_AUTHORIZATION), forHTTPHeaderField: AUTHORIZATION)
        request.addValue(getPreferenceString(PREF_LANG), forHTTPHeaderField: LANG)
        request.addValue(getPreferenceString(PREF_LOGIN_TYPE), forHTTPHeaderField: LOGIN_TYPE)
        request.addValue("ios", forHTTPHeaderField: APP_OS)
        request.addValue(UIDevice.current.systemVersion, forHTTPHeaderField: APP_OS_VERSION)
        request.addValue(UIDevice.current.model, forHTTPHeaderField: APP_MODEL)
        request.httpBody = param.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        return Alamofire.request(request).responseString(completionHandler:  {
            response in
            
            switch response.result {
            case .success:
                self.printRequestInfo(url, (request.httpMethod?.description)!, param.description, response.result, response.result.value)
                if let value = response.result.value {
                    
                    let json = JSON(value)
                    if json["code"].stringValue == "0000" {
                        target.requestFinished!(requestName, value)
                    } else {
                        target.requestResultFail!(requestName, value)
                    }
                }
                
            case .failure(let error):
                self.printRequestInfo(url, (request.httpMethod?.description)!, param.description, response.result, error)
                target.requestFailed!(requestName)
            }
        })
    }
    
    func requestMultipartFormdata(_ requestName: String, _ param: Dictionary<String, Any>, _ imageData: [Data], target: NetManagerDelegate!) {
        
        let serverUrl: String = API_SERVER
        let url = serverUrl + requestName
        let params: Dictionary<String, Any> = param
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            for data in imageData {
                multipartFormData.append(data, withName: "biz_photo", fileName: "uploadImage.jpg", mimeType: "image/jpg")
            }
            for (key, value) in params {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
        }, to: url, method:.post, headers: getAuthHeader()) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    println("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    self.printRequestInfo(url, "multipartFormData", param.description, response.result, response.result.value)
                    if let value = response.result.value {
                        
                        let json = JSON(value)
                        if json["error"] == JSON.null {
                            target.requestFinished!(requestName, value)
                        } else {
                            target.requestResultFail!(requestName, value)
                        }
                    }
                }
                
            case .failure(let encodingError):
                self.printRequestInfo(url, "multipartFormData", param.description, result, encodingError)
                target.requestFailed!(requestName)
            }
        }
    }
    
    // MARK: - log
    func printRequestInfo(_ url: String, _ method: String, _ params: String, _ result: Any!, _ value: Any!) {
        
        var message: String = "\n\n"
        message += "/*————————————————-————————————————-————————————————-"
        message += "\n|                    HTTP REQUEST                    |"
        message += "\n—————————————————————————————————-————————————————---*/"
        message += "\n"
        message += "* METHOD : \(method)"
        message += "\n"
        message += "* URL : \(url)"
        message += "\n"
        message += "* PARAM : \(params)"
        message += "\n"
        message += "* STATUS CODE : \(result!)"
        message += "\n"
        message += "* RESPONSE : \n\(JSON(value).description)"
        message += "\n"
        message += "/*————————————————-————————————————-————————————————-"
        message += "\n|                    RESPONSE END                     |"
        message += "\n—————————————————————————————————-————————————————---*/"
        println(message)
    }
}

