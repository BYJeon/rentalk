//
//  RequestStatusDetailTableViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 05/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AWSS3

@available(iOS 10.0, *)
class RequestStatusDetailTableViewController: BaseTableViewController {

    @IBOutlet var driverIDLabel: UILabel!
    @IBOutlet var driverNameLabel: UILabel!
    
    
    @IBOutlet var driverImage: UIImageView!
    @IBOutlet var driverProfileNameLabel: UILabel!
    
    @IBOutlet var pathImage: UIImageView!
    
    @IBOutlet var servicePayLabel: UILabel!
    @IBOutlet var discountLabel: UILabel!
    @IBOutlet var totalPayLabel: UILabel!
    
    @IBOutlet var pathInfoView: UIView!
    @IBOutlet var pathViaView: UIView!
    
    //경유지가 없는 경로
    @IBOutlet var targetPath: UITextView!
    @IBOutlet var sourcePath: UITextView!
    @IBOutlet var contractCompleteTimeLabel: UILabel!
    
    //경유지가 있는 경로
    @IBOutlet var targetViaPath: UITextView!
    @IBOutlet var viaPath: UITextView!
    @IBOutlet var sourceViaPath: UITextView!
    
    @IBOutlet var carNumberLabel: UILabel!
    @IBOutlet var carNameLabel: UILabel!
    
    private let infoManager = InfoManager.getInstance()
    private var reqInfo : RequestInfo!
    private var driverNumber = ""
    
    @IBOutlet var contractViaCompleteTimeLabel: UILabel!
    
    private var downloadProgress : Float = 0
    @objc var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
    
    
    //경유지 없는 탁송 요청 정보
    
    @IBOutlet var targetImg: UIImageView!
    @IBOutlet var sourceImg: UIImageView!
    @IBOutlet var missionView: UIView!
    
    @IBOutlet var targetTimeLabel: UILabel!
    @IBOutlet var sourceTimeLabel: UILabel!
    
    //경유지 탁송 요청 정보
    @IBOutlet var targetViaImg: UIImageView!
    @IBOutlet var viaImg: UIImageView!
    @IBOutlet var sourceViaImg: UIImageView!
    
    @IBOutlet var targetViaTimeLabel: UILabel!
    @IBOutlet var viaTimeLabel: UILabel!
    @IBOutlet var sourceViaTimeLabel: UILabel!
    @IBOutlet var viaMissionView: UIView!
    
    private var missionContent = ""
    
    
    @IBOutlet var cellDriverPosLabel: UILabel!
    @IBOutlet var cellContactLabel: UILabel!
    @IBOutlet var cellLastLabel: UILabel!
    
    @objc lazy var transferUtility = {
        AWSS3TransferUtility.default()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        driverIDLabel.text = ""
        driverNameLabel.text = ""
        
        pathInfoView.isHidden = true
        pathViaView.isHidden = true
        
        reqInfo = infoManager.requestInfo!
        if reqInfo.state == "2" { //탁송 완료
            request(API_DRIVER_END_INFO_REQUEST)
        }else if reqInfo.state == "1"{ //탁송 중
            request(API_DRIVER_REQUEST)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var totalCnt = 17
        if reqInfo.state == "2" && missionContent.count == 0 {
            totalCnt = 14
            cellDriverPosLabel.text = "공유하기"
        }else if reqInfo.state == "2" && missionContent.count != 0 {
            totalCnt = 15
            cellDriverPosLabel.text = "체결 계약 조회하기"
            cellContactLabel.text = "공유하기"
        }else if reqInfo.state != "2" && missionContent.count == 0 {
            totalCnt = 15
            cellContactLabel.text = "공유하기"
        }else if reqInfo.state != "2" && missionContent.count != 0 {
            cellDriverPosLabel.text = "드라이버 현재위치 보기"
            cellContactLabel.text = "체결 계약 조회하기"
            totalCnt = 16
        }
        
        return totalCnt
    }
    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if reqInfo.state == "2" && indexPath.row == 13 {
//            // remove the second cell by skipping it and returning the third cell in its place
//            return super.tableView(tableView, cellForRowAt: IndexPath(row: indexPath.row + 1, section: 0))
//        }else if reqInfo.state == "2" && indexPath.row == 14 {
//            return super.tableView(tableView, cellForRowAt: IndexPath(row: indexPath.row + 1, section: 0))
//        }
//        return super.tableView(tableView, cellForRowAt: indexPath)
//    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        if indexPath.row == 11 { //드라이버에게 전화하기
            sendCall(driverNumber)
        }else if indexPath.row == 12 { //고객센터에 전화하기
            sendCall("16446033")
        }else if indexPath.row == 13 { //드라이버 현재 위치 보기
            if cellDriverPosLabel.text == "드라이버 현재위치 보기"{
                if parentDelegate != nil {
                    parentDelegate?.moveToPagePresentFromDelegate!("DriverPosViewController", title: driverIDLabel.text!, urlInfo: driverIDLabel.text!)
                }
            }else if cellDriverPosLabel.text == "체결 계약 조회하기"{
                if pathViaView.isHidden == false {
                    if sourceViaTimeLabel.text!.count == 0 {
                        if parentDelegate != nil {
                            parentDelegate?.showPopupViewFromDelegate("체결 계약 조회하기", "계약서 작성이 완료되지 않았습니다.")
                            return
                        }
                    }
                }else if pathInfoView.isHidden == false {
                    if sourceTimeLabel.text!.count == 0 {
                        if parentDelegate != nil {
                            parentDelegate?.showPopupViewFromDelegate("체결 계약 조회하기", "계약서 작성이 완료되지 않았습니다.")
                            return
                        }
                    }
                }
                
                if parentDelegate != nil {
                    parentDelegate?.moveToPagePresentFromDelegate?("ContractViewController", title: driverIDLabel.text!, urlInfo: driverIDLabel.text!)
                }
            }else{
                println("공유하기")
                shareImage()
            }
            
        }else if indexPath.row == 14 { //공유하기
            if cellContactLabel.text == "드라이버 현재위치 보기"{
                if parentDelegate != nil {
                    parentDelegate?.moveToPagePresentFromDelegate!("DriverPosViewController", title: driverIDLabel.text!, urlInfo: driverIDLabel.text!)
                }
            }else if cellContactLabel.text == "체결 계약 조회하기"{
                if parentDelegate != nil {
                    parentDelegate?.moveToPagePresentFromDelegate?("ContractViewController", title: driverIDLabel.text!, urlInfo: driverIDLabel.text!)
                }
            }else{ //공유하기
                println("공유하기")
                shareImage()
            }
        }else if indexPath.row == 15 { //공유하기
            println("공유하기")
            shareImage()
        }
    }
    
    func screenshot() -> UIImage{
    var image = UIImage();
        UIGraphicsBeginImageContextWithOptions(self.tableView.contentSize, false, UIScreen.main.scale)

        // save initial values
        let savedContentOffset = self.tableView.contentOffset;
        let savedFrame = self.tableView.frame;
        let savedBackgroundColor = self.tableView.backgroundColor

        // reset offset to top left point
        self.tableView.contentOffset = CGPoint(x: 0, y: 0);
        // set frame to content size
        self.tableView.frame = CGRect(x: 0, y: 0, width: self.tableView.contentSize.width, height: self.tableView.contentSize.height);
        // remove background
        self.tableView.backgroundColor = UIColor.clear

        // make temp view with scroll view content size
        // a workaround for issue when image on ipad was drawn incorrectly
        let tempView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.contentSize.width, height: self.tableView.contentSize.height));

        // save superview
        let tempSuperView = self.tableView.superview
        // remove scrollView from old superview
        self.tableView.removeFromSuperview()
        // and add to tempView
        tempView.addSubview(self.tableView)

        // render view
        // drawViewHierarchyInRect not working correctly
        tempView.layer.render(in: UIGraphicsGetCurrentContext()!)
        // and get image
        image = UIGraphicsGetImageFromCurrentImageContext()!;

        // and return everything back
        tempView.subviews[0].removeFromSuperview()
        tempSuperView?.addSubview(self.tableView)

        // restore saved settings
        self.tableView.contentOffset = savedContentOffset;
        self.tableView.frame = savedFrame;
        self.tableView.backgroundColor = savedBackgroundColor

        UIGraphicsEndImageContext();

        return image
    }
    
    private func shareImage(){
        let img: UIImage = screenshot()
        let items = [img]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
        
//        if parentDelegate != nil {
//            parentDelegate?.takeScreenShot?()
//        }
    }
    private func sendCall(_ number: String){
        if let phoneCallURL = URL(string: "tel://\(number)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
            
        }
    }
    
    private func downloadDriverProfile(_ imgPath: String, isProfile: Bool){
        if imgPath.count < 2 { return }
        
        let expression = AWSS3TransferUtilityDownloadExpression()
        expression.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                if (self.downloadProgress < Float(progress.fractionCompleted)) {
                    self.downloadProgress = Float(progress.fractionCompleted)
                }
            })
        }
        
        self.completionHandler = { (task, location, data, error) -> Void in
            DispatchQueue.main.async(execute: {
                if let error = error {
                    NSLog("Failed with error: \(error)")
                    //self.statusLabel.text = "Failed"
                }
                else if(self.downloadProgress != 1.0) {
                    //self.statusLabel.text = "Failed"
                }
                else{
                    if isProfile { // 기사 프로필 이미지
                        self.driverImage.image = UIImage(data: data!)
                    }else{ // Path 정보
                        self.pathImage.image = UIImage(data: data!)
                    }
                }
            })
        }
        
        transferUtility.downloadData(
            forKey: imgPath,
            expression: expression,
            completionHandler: completionHandler).continueWith { (task) -> AnyObject? in
                if let error = task.error {
                    NSLog("Error: %@",error.localizedDescription);
                    DispatchQueue.main.async(execute: {
                        //self.statusLabel.text = "Failed"
                    })
                }
                
                if let _ = task.result {
                    DispatchQueue.main.async(execute: {
                        //self.statusLabel.text = "Downloading..."
                    })
                    NSLog("Download Starting!")
                    // Do something with uploadTask.
                }
                return nil;
        }
    }
    //MARK: - network
    override func request(_ name: String) {
        switch name {
        case API_DRIVER_END_INFO_REQUEST: //탁송 완료 시점 정보
            let reqInfo = infoManager.requestInfo
            let extraParam : String = "/" + reqInfo!.req_id + "?api_token="+infoManager.getApiToken()
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
        case API_DRIVER_REQUEST:
            let reqInfo = infoManager.requestInfo
            let extraParam : String = "/" + reqInfo!.req_id + "?api_token="+infoManager.getApiToken()
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        switch requestID {
        case API_DRIVER_END_INFO_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                
                let way_point_addr   = json["waypoint_info"]["address"].stringValue
                let start_point_addr = json["start_point_info"]["address"].stringValue
                let end_point_addr   = json["end_point_info"]["address"].stringValue
                
                let way_point_time = json["waypoint_info"]["arrive_time"].stringValue
                let start_point_time = json["start_point_info"]["arrive_time"].stringValue
                let end_point_time = json["end_point_info"]["arrive_time"].stringValue
                
                missionContent = json["mission_info"][0]["mission_nm"].stringValue
                
                if way_point_addr.count != 0 {
                    pathViaView.isHidden = false
                    pathInfoView.isHidden = true
                    targetViaPath.text = start_point_addr
                    sourceViaPath.text = end_point_addr
                    viaPath.text = way_point_addr
                    
                    targetViaTimeLabel.text = start_point_time
                    sourceViaTimeLabel.text = end_point_time
                    viaTimeLabel.text    = way_point_time
                    
                    if missionContent.count > 0 {
                        missionView.isHidden = true
                        viaMissionView.isHidden = false
                        contractViaCompleteTimeLabel.isHidden = false
                        contractViaCompleteTimeLabel.text = json["mission_info"][0]["completed_time"].stringValue
                    }else{
                        missionView.isHidden = true
                        viaMissionView.isHidden = true
                        contractViaCompleteTimeLabel.isHidden = true
                        contractCompleteTimeLabel.isHidden = true
                    }
                    
                }else{
                    pathViaView.isHidden = true
                    pathInfoView.isHidden = false
                    
                    targetPath.text = start_point_addr
                    sourcePath.text = end_point_addr
                    
                    targetTimeLabel.text = start_point_time
                    sourceTimeLabel.text = end_point_time
                    
                    if missionContent.count > 0 {
                        missionView.isHidden = false
                        viaMissionView.isHidden = true
                        contractCompleteTimeLabel.isHidden = false
                        contractCompleteTimeLabel.text = json["mission_info"][0]["completed_time"].stringValue
                    }else{
                        missionView.isHidden = true
                        viaMissionView.isHidden = true
                        contractViaCompleteTimeLabel.isHidden = true
                        contractCompleteTimeLabel.isHidden = true
                    }
                }
                
                
                driverIDLabel.text = json["req_id"].stringValue
                driverNameLabel.text = json["driver_info"]["driver_nm"].stringValue
                
                servicePayLabel.text = json["fee_info"]["service_fee"].stringValue.getCurrencyString() + " 원"
                discountLabel.text = json["fee_info"]["total_dc"].stringValue.getCurrencyString() + " 원"
                totalPayLabel.text = json["fee_info"]["charge_fee"].stringValue.getCurrencyString() + " 원"
                
                
                let profile  = json["driver_info"]["profile_photo"].stringValue
                let pathInfo = json["path_img"].stringValue
                
                driverProfileNameLabel.text = json["driver_info"]["driver_nm"].stringValue
                carNumberLabel.text = json["car_info"]["car_no"].stringValue
                carNameLabel.text = json["car_info"]["car_mdl"].stringValue
                driverNumber = json["driver_info"]["tel_no"].stringValue
                if profile.count > 1 {
                    downloadDriverProfile(profile, isProfile: true)
                }
                if pathInfo.count > 1 {
                    
                    let pre = pathInfo.index(pathInfo.startIndex, offsetBy: 0)
                    let prefix = String(pathInfo[pre])
                    
                    if prefix == "r"{
                        self.downloadDriverProfile(pathInfo, isProfile: false)
                    }else{
                        let startIndex = pathInfo.index(pathInfo.startIndex, offsetBy: 1)
                        self.downloadDriverProfile(String(pathInfo[startIndex...]), isProfile: false)
                    }
                    
                    //let startIndex = pathInfo.index(pathInfo.startIndex, offsetBy: 1)
                    //self.downloadDriverProfile(String(pathInfo[startIndex...]), isProfile: false)
                }
                
            }
            
            tableView.reloadData()
            break
            
        case API_DRIVER_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                let way_point_addr   = json["waypoint_info"]["address"].stringValue
                let start_point_addr = json["start_point_info"]["address"].stringValue
                let end_point_addr   = json["end_point_info"]["address"].stringValue
                
                let way_point_time = json["waypoint_info"]["arrive_time"].stringValue
                let start_point_time = json["start_point_info"]["arrive_time"].stringValue
                let end_point_time = json["end_point_info"]["arrive_time"].stringValue
                
                missionContent = json["mission_info"][0]["mission_nm"].stringValue
                
                if way_point_addr.count != 0 {
                    pathViaView.isHidden = false
                    pathInfoView.isHidden = true
                    targetViaPath.text = start_point_addr
                    sourceViaPath.text = end_point_addr
                    viaPath.text = way_point_addr
                    
                    targetViaTimeLabel.text = start_point_time
                    sourceViaTimeLabel.text = end_point_time
                    viaTimeLabel.text    = way_point_time
                    
                    targetViaImg.image = UIImage(named: start_point_time.count == 0 ? "ico_map_location_n" : "ico_map_location_p")
                    sourceViaImg.image = UIImage(named: end_point_time.count == 0 ? "ico_map_location_n" : "ico_map_location_p")
                    viaImg.image = UIImage(named: way_point_time.count == 0 ? "ico_map_location_n" : "ico_map_location_p")
                    
                    if missionContent.count > 0 {
                        missionView.isHidden = true
                        viaMissionView.isHidden = false
                        contractViaCompleteTimeLabel.isHidden = false
                        contractViaCompleteTimeLabel.text = json["mission_info"][0]["completed_time"].stringValue
                    }else{
                        missionView.isHidden = true
                        viaMissionView.isHidden = true
                        contractViaCompleteTimeLabel.isHidden = true
                        contractCompleteTimeLabel.isHidden = true
                    }
                    
                }else{
                    pathViaView.isHidden = true
                    pathInfoView.isHidden = false
                    
                    targetPath.text = start_point_addr
                    sourcePath.text = end_point_addr
                    
                    targetTimeLabel.text = start_point_time
                    sourceTimeLabel.text = end_point_time
                    
                    targetImg.image = UIImage(named: start_point_time.count == 0 ? "ico_map_location_n" : "ico_map_location_p")
                    sourceImg.image = UIImage(named: end_point_time.count == 0 ? "ico_map_location_n" : "ico_map_location_p")
                    
                    if missionContent.count > 0 {
                        missionView.isHidden = false
                        viaMissionView.isHidden = true
                        contractCompleteTimeLabel.isHidden = false
                        contractCompleteTimeLabel.text = json["mission_info"][0]["completed_time"].stringValue
                    }else{
                        missionView.isHidden = true
                        viaMissionView.isHidden = true
                        contractViaCompleteTimeLabel.isHidden = true
                        contractCompleteTimeLabel.isHidden = true
                    }
                }
                
                driverIDLabel.text = json["req_id"].stringValue
                driverNameLabel.text = json["driver_info"]["driver_nm"].stringValue
                
                servicePayLabel.text = json["fee_info"]["service_fee"].stringValue.getCurrencyString() + " 원"
                discountLabel.text = json["fee_info"]["total_dc"].stringValue.getCurrencyString() + " 원"
                totalPayLabel.text = json["fee_info"]["charge_fee"].stringValue.getCurrencyString() + " 원"
                
                let profile  = json["driver_info"]["profile_photo"].stringValue
                let pathInfo = json["path_img"].stringValue
                
                driverProfileNameLabel.text = json["driver_info"]["driver_nm"].stringValue
                carNumberLabel.text = json["car_info"]["car_no"].stringValue
                carNameLabel.text = json["car_info"]["car_mdl"].stringValue
                driverNumber = json["driver_info"]["tel_no"].stringValue
                if profile.count > 1 {
                    downloadDriverProfile(profile, isProfile: true)
                }
                if pathInfo.count > 1 {
                    
                    let pre = pathInfo.index(pathInfo.startIndex, offsetBy: 0)
                    let prefix = String(pathInfo[pre])
                    
                    if prefix == "r"{
                        self.downloadDriverProfile(pathInfo, isProfile: false)
                    }else{
                        let startIndex = pathInfo.index(pathInfo.startIndex, offsetBy: 1)
                        self.downloadDriverProfile(String(pathInfo[startIndex...]), isProfile: false)
                    }
                    
                    //let startIndex = pathInfo.index(pathInfo.startIndex, offsetBy: 1)
                    //self.downloadDriverProfile(pathInfo, isProfile: false)
                }
            }
            
            tableView.reloadData()
            break
        default:
            break
        }
        
        if parentDelegate != nil {
            parentDelegate?.childViewRequestComplete?()
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        if parentDelegate != nil {
            parentDelegate?.childViewRequestComplete?()
        }
        
        let dialog = UIAlertController(title: "탁송현황 오류", message: "관리자에게 문의해 주세요.", preferredStyle: .alert)
        let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
        dialog.addAction(action)
        self.present(dialog, animated: true, completion: nil)
        //showPopupView("드라이버 호출 오류", "관리자에게 문의해 주세요.")
    }
}
