//
//  LoginViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 07/08/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Mixpanel

class LoginViewController: BaseSwipeViewController, UITextFieldDelegate {

    @IBOutlet var registerBtn: UIButton!
    
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var secureTextField: UITextField!
    
    @IBOutlet var loginGuideView: UIView!
    @IBOutlet var loginGuideBtn: UIButton!
    
    @IBOutlet var dimView: UIView!
    @IBOutlet var popupView: UIView!
    @IBOutlet var popupTitleLabel: UILabel!
    @IBOutlet var popupContentLabel: UITextView!
    
    //timer
    private var mTimer : Timer?
    private var remainTime = 180
    @IBOutlet var secureTimeLabel: UILabel!
    
    @IBOutlet var inputBottomContraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let border = CALayer()
        border.backgroundColor = UIColor.black.cgColor
        border.frame = CGRect(x: 0, y: (registerBtn?.frame.size.height)! - 3, width: (registerBtn?.frame.size.width)!, height: 1)
        registerBtn?.layer.addSublayer(border)
        
        phoneTextField.delegate = self
        secureTextField.delegate = self
        
        phoneTextField.keyboardType = .numbersAndPunctuation
        secureTextField.keyboardType = .numbersAndPunctuation
        // Touch Event 등록 후 함수를 연동한다. (getPosByMap)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dimDidClicked(_:)))
        self.dimView.addGestureRecognizer(gesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willResignActive), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.inputBottomContraint.constant = 4
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: self.view.window)
    }
    
    //DimView Clicked
    @objc func dimDidClicked(_ sender: UIGestureRecognizer){
        hidePopupView()
    }
    
    //타이머가 호출하는 콜백함수
    @objc func timerCallback(){
        if remainTime > 0 {
            remainTime -= 1
            secureTimeLabel.text = String(format: "%02d:%02d", remainTime/60, remainTime%60)
        }else{
            secureTimeLabel.isHidden = true
            mTimer?.invalidate()
        }
        
    }
    
    @objc func willResignActive(_ notification: Notification) {
        print("Background");
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification:NSNotification) {
        print("Show");
        
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        print(keyboardHeight)
        
        adjustingHeight(show: true, notification: notification)
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        print("Hide")
        adjustingHeight(show: false, notification: notification)
    }
    
    func adjustingHeight(show:Bool, notification:NSNotification) {
        
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let animationDurarion = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        //let changeInHeight = (keyboardFrame.height) * (show ? 1 : -1)
        let changeInHeight = CGFloat(keyboardRectangle.height / 2) * (show ? 1 : -1)
        UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
            self.inputBottomContraint.constant += changeInHeight
        })
        
    }
    
    private func showPopupView(_ title: String, _ content: String ){
        self.view.endEditing(true)
        
        popupTitleLabel.text = title
        popupContentLabel.text = content
        
        popupView.isHidden = false
        dimView.isHidden = false
    }
    private func hidePopupView(){
        popupView.isHidden = true
        dimView.isHidden = true
    }
    
    private func showGuideView(_ isShow: Bool){
        if isShow {
            loginGuideBtn.setImage(UIImage(named: "ico_phone_guide_p"), for: .normal)
            loginGuideView.isHidden = false
            loginGuideBtn.isSelected = true
        }else{
            loginGuideBtn.isSelected = false
            loginGuideBtn.setImage(UIImage(named: "ico_phone_guide"), for: .normal)
            loginGuideView.isHidden = true
        }
    }
    @IBAction func loginDidClicked(_ sender: Any) {
        showGuideView(false)
        request(API_USER_LOGIN)
    }
    
    @IBAction func secureRequestDidClicked(_ sender: Any) {
        showGuideView(false)
        
        request(API_USER_LOGIN_REQ_NUM)
        
        remainTime = 180
        secureTimeLabel.isHidden = false
        if let timer = mTimer {
            //timer 객체가 nil 이 아닌경우에는 invalid 상태에만 시작한다
            if !timer.isValid {
                /** 1초마다 timerCallback함수를 호출하는 타이머 */
                mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
            }
        }else{
            //timer 객체가 nil 인 경우에 객체를 생성하고 타이머를 시작한다
            /** 1초마다 timerCallback함수를 호출하는 타이머 */
            mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
        }
    }
    
    @IBAction func loginGuideDidClicked(_ sender: Any) {
        loginGuideBtn.isSelected = !loginGuideBtn.isSelected
        showGuideView(loginGuideBtn.isSelected)
    }
    
    @IBAction func popupConfirmDidClicked(_ sender: Any) {
        hidePopupView()
    }
    
    //MARK: - network
    override func request(_ name: String) {
        switch name {
        case API_USER_LOGIN:
            let phone : String = phoneTextField.trimmingCharacters()
            let secure : String = secureTextField.trimmingCharacters()
            let token : String  = InfoManager.getInstance().getToken()
            
            let params: Parameters = [
                "emp_id" : phone
                ,"auth_no" : secure
                ,"fcm_token" : token
                ,"os_gb" : "1"
            ]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.put, target: self)
            break
            
        case API_USER_LOGIN_REQ_NUM:
            let phone : String = phoneTextField.trimmingCharacters()
            let params: Parameters = ["tel_no" : phone]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.post, target: self)
            break;
            
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        switch requestID {
        case API_USER_LOGIN:
            let phone : String = phoneTextField.trimmingCharacters()
            
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                #if RELEASE
                Mixpanel.mainInstance().track(event: "LOGIN",
                                              properties:["METHOD": "TYPING"])
                #endif
                
                InfoManager.getInstance().setApiToken(token: json["api_token"].stringValue)
                InfoManager.getInstance().setAppID(id: phone)
                InfoManager.getInstance().companyTypeGb = json["company_type_gb"].stringValue
                InfoManager.getInstance().setCompanyType(type: json["company_type_gb"].stringValue)
                
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuController")
                UIApplication.shared.keyWindow?.rootViewController = viewController
            }
            break
            
        case API_USER_LOGIN_REQ_NUM:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                showPopupView("인증번호 요청 완료", "인증번호를 확인해 주세요")
                
                //let dialog = UIAlertController(title: "인증번호 요청 완료", message: "인증번호를 확인해 주세요", preferredStyle: .alert)
                //let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
                //dialog.addAction(action)
                //self.present(dialog, animated: true, completion: nil)
            }else if json["success"].stringValue == "200"{
                showPopupView("인증번호 요청", "회원 등록 전 입니다.")
            }else if json["success"].stringValue == "103"{
                showPopupView("인증번호 요청", "접수가 요청 되었습니다.")
            }
            break;
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        let json = JSON(data!)
        
        //let dialog = UIAlertController(title: "로그인 오류", message: "오류 번호 : " + json["success"].stringValue, preferredStyle: .alert)
        //let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
        //dialog.addAction(action)
        //self.present(dialog, animated: true, completion: nil)
        secureTimeLabel.isHidden = true
        showPopupView("로그인 오류", json["tel_no"][0].stringValue)
    }
    
    
    /// TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        showGuideView(false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
