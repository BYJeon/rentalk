//
//  CustomInfoWindow.swift
//  rentalk
//
//  Created by Yuna Daddy on 06/08/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import NMapsMap

class CustomInfoWindow: UIView, NMFOverlayImageDataSource {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var bgImg: UIImageView!
    
    private var title: String!
    
    func view(with overlay: NMFOverlay) -> UIView {
        let customInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)?[0] as! CustomInfoWindow
        customInfoWindow.titleLabel.text = self.title
        if self.title == "출발"{
            customInfoWindow.bgImg.image = UIImage(named: "ico_map_pin_g")
            customInfoWindow.titleLabel.textColor = UIColor.white
        }
        return customInfoWindow
    }
    
    required init(title: String) {
        super.init(frame: .zero)
        self.title = title
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
