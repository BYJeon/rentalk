//
//  BaseTableTableViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 05/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController, NetManagerDelegate {
    internal var parentDelegate : ParentViewDelegate? = nil
    internal let pathInfoManager : PathInfoManager = PathInfoManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
    
    //MARK: - NetManager delegate
    func request(_ name: String) {
        println(">>> requestID = \(name)")
    }
    
    func requestFinished(_ requestID: String, _ data: Any!) {
    }
    
    func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
    }
    
    func requestFailed(_ requestID: String) {
        println(">>> requestID = \(requestID)")
    }
    
}
