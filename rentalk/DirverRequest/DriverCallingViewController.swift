//
//  DriverCallingViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 05/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AWSS3

enum DriverReqType {
    case NORMAL
    case NOT_FOUND
    case FOUNDED
}

class DriverCallingViewController: BaseSwipeViewController {

    @IBOutlet var pathInfoView: UIView!
    @IBOutlet var pathViaInfoView: UIView!
    
    //경유지가 없는 경로
    @IBOutlet var targetPath: UITextView!
    @IBOutlet var sourcePath: UITextView!
    
    //경유지가 있는 경로
    @IBOutlet var targetViaPath: UITextView!
    @IBOutlet var viaPath: UITextView!
    @IBOutlet var sourceViaPath: UITextView!
    
    private let pathInfoManager : PathInfoManager = PathInfoManager.sharedInstance
    private let infoManager : InfoManager = InfoManager.getInstance()
    
    @IBOutlet var totalPayLabel: UILabel!
    @IBOutlet var discountPayLabel: UILabel!
    
    private var driverReqType = DriverReqType.NORMAL
    
    //popupview
    @IBOutlet var dimView: UIView!
    @IBOutlet var popupView: UIView!
    @IBOutlet var popupTitleLabel: UILabel!
    @IBOutlet var popupContentLabel: UITextView!
    
    //AWS S3
    private var downloadProgress : Float = 0
    @objc var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
    
    @objc lazy var transferUtility = {
        AWSS3TransferUtility.default()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //경유지가 존재할 경우
//        if pathInfoManager.isHasRepairShopInfo() {
//            pathViaInfoView.isHidden = false
//            pathInfoView.isHidden = true
//
//            targetViaPath.text = pathInfoManager.getTargetInfo()!.getAddress()
//            sourceViaPath.text = pathInfoManager.getSourceInfo()!.getAddress()
//            viaPath.text      = pathInfoManager.getRepariShopInfo()!.getAddress()
//        }else{
//            pathViaInfoView.isHidden = true
//            pathInfoView.isHidden = false
//
//            targetPath.text = pathInfoManager.getTargetInfo()!.getAddress()
//            sourcePath.text   = pathInfoManager.getSourceInfo()!.getAddress()
//        }
        // 경유지가 존재할 경우
        if infoManager.way_point_address.count > 0 {
            pathViaInfoView.isHidden = false
            pathInfoView.isHidden = true
            
            targetViaPath.text = infoManager.start_point_address
            sourceViaPath.text = infoManager.end_point_address
            viaPath.text      = infoManager.way_point_address
        }else{
            pathViaInfoView.isHidden = true
            pathInfoView.isHidden = false
            
            targetPath.text = infoManager.start_point_address
            sourcePath.text  = infoManager.end_point_address
        }
        
        let ext_amt: String = infoManager.expect_amt
        let dc_amt: String  = infoManager.dc_amt
        
        totalPayLabel.text = "예상 총 이용료 : " + ext_amt.getCurrencyString() + "원"
        discountPayLabel.text = "특별 이벤트 할인 : " + dc_amt.getCurrencyString() + "원"
        
        // Touch Event 등록 후 함수를 연동한다. (getPosByMap)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dimDidClicked(_:)))
        self.dimView.addGestureRecognizer(gesture)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(driverFounded), name: NSNotification.Name(rawValue: NOTI_DRIVER_FOUNDED), object: nil)
        //드라이버 찾을 수 없음 noti
        NotificationCenter.default.addObserver(self, selector: #selector(driverNotFound), name: NSNotification.Name(rawValue: NOTI_DRIVER_NOT_FOUND), object: nil)
    }
    
    @IBAction func popupConfirmDidClicked(_ sender: Any) {
        if driverReqType == .NOT_FOUND{
            dismiss(animated: true, completion: nil)
        }else{
            hidePopupView()
        }
    }
    //DimView Clicked
    @objc func dimDidClicked(_ sender: UIGestureRecognizer){
        if driverReqType == .NOT_FOUND{
            dismiss(animated: true, completion: nil)
        }else{
            hidePopupView()
        }
    }
    
    @objc func driverFounded() {
        //드라이버 정보 요청
        request(API_DRIVER_INFO_REQUEST)
    }
    
    @objc func driverNotFound() {
        driverReqType = DriverReqType.NOT_FOUND
        if dimView.isHidden {
            showPopupView("탁송요청 실패", "수행가능한 드라이버가 없습니다.")
        }
    }
    
    
    private func showPopupView(_ title: String, _ content: String){
        popupTitleLabel.text = title
        popupContentLabel.text = content
        
        popupView.isHidden = false
        dimView.isHidden = false
    }
    private func hidePopupView(){
        popupView.isHidden = true
        dimView.isHidden = true
    }
    
    
    private func downloadDriverProfile(){
        infoManager.driverInfo!.dirver_image = UIImage(named: "img_profile_none")
        
        if self.infoManager.driverInfo!.profile_photo.count == 0 {return}
        
        let expression = AWSS3TransferUtilityDownloadExpression()
        expression.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                if (self.downloadProgress < Float(progress.fractionCompleted)) {
                    self.downloadProgress = Float(progress.fractionCompleted)
                }
            })
        }
        
        self.completionHandler = { (task, location, data, error) -> Void in
            DispatchQueue.main.async(execute: {
                if let error = error {
                    NSLog("Failed with error: \(error)")
                    //self.statusLabel.text = "Failed"
                    self.infoManager.driverInfo!.dirver_image = UIImage(named: "img_profile_none")
                }
                else if(self.downloadProgress != 1.0) {
                    //self.statusLabel.text = "Failed"
                }
                else{
                    self.infoManager.driverInfo!.dirver_image = UIImage(data: data!)
                }
                
                if self.mainOperationDelegate != nil {
                    self.mainOperationDelegate?.popToRootViewConterller()
                }
            })
        }
        
        transferUtility.downloadData(
            forKey: self.infoManager.driverInfo!.profile_photo,
            expression: expression,
            completionHandler: completionHandler).continueWith { (task) -> AnyObject? in
                if let error = task.error {
                    NSLog("Error: %@",error.localizedDescription);
                    DispatchQueue.main.async(execute: {
                        //self.statusLabel.text = "Failed"
                    })
                }
                
                if let _ = task.result {
                    DispatchQueue.main.async(execute: {
                        //self.statusLabel.text = "Downloading..."
                    })
                    NSLog("Download Starting!")
                    // Do something with uploadTask.
                }
                return nil;
        }
    }
    // Controller Event
    //탁소 취소 하기
    @IBAction func closeDidClicked(_ sender: Any) {
        request(API_DRIVER_REQUEST_CANCLE)
    }
    
    //MARK: - network
    override func request(_ name: String) {
        switch name {
        case API_DRIVER_INFO_REQUEST:
            let extraParam : String = "/" + infoManager.reqID + "?api_token="+infoManager.getApiToken()
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
        case API_DRIVER_REQUEST_CANCLE:
            let params: Parameters = [
                "req_id" : infoManager.reqID
                ,"api_token" : infoManager.getApiToken()
            ]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.put, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        switch requestID {
        case API_DRIVER_INFO_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                infoManager.setMenuType(type: MenuType.DRIVERPICK)
                infoManager.setDriverInfo(driverID: json["driver_id"].stringValue, driver_nm: json["driver_nm"].stringValue, dist_gap: json["dist_gap"].stringValue,
                                          missions: [json["missions"].stringValue], expected_amt: json["expected_amt"].stringValue,
                                          profile_photo: json["profile_photo"].stringValue, eval_score: json["eval_score"].stringValue,
                                          start_point_la: json["start_point_la"].stringValue, start_point_lo: json["start_point_lo"].stringValue,
                                          end_point_la: json["end_point_la"].stringValue, end_point_lo: json["end_point_lo"].stringValue, waypoint_la: json["waypoint_la"].stringValue, waypoint_lo: json["waypoint_lo"].stringValue)
                
                downloadDriverProfile()
            }
            break
        case API_DRIVER_REQUEST_CANCLE:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
            }
            dismiss(animated: true, completion: nil)
            break
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        //let json = JSON(data!)
        
        //let dialog = UIAlertController(title: "로그인 오류", message: "오류 번호 : " + json["success"].stringValue, preferredStyle: .alert)
        //let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
        //dialog.addAction(action)
        //self.present(dialog, animated: true, completion: nil)
        showPopupView("드라이버 호출 오류", "관리자에게 문의해 주세요.")
    }
}
