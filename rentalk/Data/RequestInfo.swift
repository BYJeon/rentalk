//
//  RequestInfo.swift
//  rentalk
//
//  Created by Yuna Daddy on 06/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import Foundation

class RequestInfo{
    var req_id : String = ""
    var car_no : String = ""
    var car_mdl : String = ""
    var state : String = ""
    var amt : String = ""
    var date: String = ""
    var own_request : String = ""
    var path_img : String = ""
    var path_img_s3 : UIImage? = nil
    
    init(req_id: String, state: String){
        self.req_id = req_id
        self.state = state
    }
    init(req_id: String, car_no: String, car_mdl: String, state: String, amt: String, date: String, own_request: String, path_img: String) {
        self.req_id = req_id
        self.car_no = car_no
        self.car_mdl = car_mdl
        self.state = state
        self.amt = amt
        self.date = date
        self.own_request = own_request
        self.path_img = path_img
    }
}
