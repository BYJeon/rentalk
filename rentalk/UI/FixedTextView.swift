//
//  FixedTextView.swift
//  rentalk
//
//  Created by Yuna Daddy on 2019/11/06.
//  Copyright © 2019 rentalk. All rights reserved.
//

import Foundation

class FixedTextView: UITextView {
    required init?(coder: NSCoder) {
        if #available(iOS 13.2, *) {
            super.init(coder: coder)
        }
        else {
            //let rect = CGRect(origin: .zero, size: CGSize(width: getTargetWidth(), height: 44*3))
            super.init(frame: .zero, textContainer: nil)
            self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.contentMode = .scaleToFill

            self.isScrollEnabled = false   // causes expanding height

            // Auto Layout
            self.translatesAutoresizingMaskIntoConstraints = false
            self.font = UIFont(name: "HelveticaNeue", size: 16)
            self.textColor = UIColor.white
            self.backgroundColor = UIColor.clear
        }
        
        self.isUserInteractionEnabled = true
    }
}
