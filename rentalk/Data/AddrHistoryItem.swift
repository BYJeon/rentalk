//
//  AddrHistory.swift
//  rentalk
//
//  Created by Yuna Daddy on 21/10/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import Foundation

struct AddrHistoryItem: Codable {
    var poiName: String
    var addr: String
    var lat: Double
    var lgn: Double
}
