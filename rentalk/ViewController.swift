//
//  ViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 06/08/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import NMapsMap

class PathInfo{
    
}

class ViewController: UIViewController, NMFMapViewDelegate {
    private var mapView : NMFMapView? = nil
    private let tmapAPI = TmapAPI()
    private let locationManager = CLLocationManager()
    private let pathInfoManaget : PathInfoManager = PathInfoManager.sharedInstance
    private var path1 : PathItem? = nil;
    private var path2 : PathItem? = nil;
    private var path3 : PathItem? = nil;

    override func viewDidLoad() {
        super.viewDidLoad()
    
//        mapView = NMFMapView(frame: view.frame)
//        view.addSubview(mapView!)
//
//        path1 = PathItem(lat: 37.388592, lng: 127.123480, address: "1")
//        path2 = PathItem(lat: 37.498078, lng: 127.027652, address: "2")
//        path3 = PathItem(lat: 37.562804, lng: 126.826819, address: "3")
//
//        pathInfoManaget.setPathInfo(item: path1, pathType: PathType.SOURCE)
//        pathInfoManaget.setPathInfo(item: path2, pathType: PathType.TARGET)
//        pathInfoManaget.setPathInfo(item: path3, pathType: PathType.REPAIR_SHOP)
//
//        setMarkerInMap(lat: 37.388592, lng: 127.123480, title: "출발")
//        setMarkerInMap(lat: 37.498078, lng: 127.027652, title: "도착")
//        setMarkerInMap(lat: 37.562804, lng: 126.826819, title: "경유")
//
//
//        mapView?.delegate = self
//
//        let southWestPos = pathInfoManaget.getSouthWestPathInfo()
//        let northEastPos = pathInfoManaget.getNorthEastPathInfo()
//
//        let bounds = NMGLatLngBounds(southWestLat: southWestPos.getLatInfo(), southWestLng: southWestPos.getLngInfo(), northEastLat: northEastPos.getLatInfo(), northEastLng: northEastPos.getLngInfo())
//        let cameraUpdate = NMFCameraUpdate(fit: bounds, padding: 50)
//        mapView!.moveCamera(cameraUpdate)
        
        //배터리 소모가 심해도 정확도를 높힌다.
        //locationManager.desiredAccuracy = kCLLocationAccuracyBest
        //locationManager.requestAlwaysAuthorization()
        //locationManager.startUpdatingLocation()
        //locationManager.delegate = self
        //move(at: locationManager.location?.coordinate)
        
        //let dis1 = calcDistance(lat1: (path1?.getLatInfo())!, lon1: (path1?.getLngInfo())!, lat2: 37.244980, lon2: 131.868397)
        //let dis2 = calcDistance(lat1: (path2?.getLatInfo())!, lon1: (path2?.getLngInfo())!, lat2: 37.244980, lon2: 131.868397)
        //let dis3 = calcDistance(lat1: (path3?.getLatInfo())!, lon1: (path3?.getLngInfo())!, lat2: 37.244980, lon2: 131.868397)
        
    }
    

    func setMarkerInMap(lat: Double, lng: Double, title: String){
        let marker = NMFMarker()
        marker.position = NMGLatLng(lat: lat, lng: lng)
        let markerImg = title == "출발" ? "ico_map_pincircle_g" : "ico_map_pincircle_y"
        marker.iconImage = NMFOverlayImage(name: markerImg)
        marker.mapView = mapView
        
        let infoWindow = NMFInfoWindow()
        let customInfo = CustomInfoWindow(title: title)
        infoWindow.dataSource = customInfo
        infoWindow.open(with: marker)
    }
    
    func mapView(_ mapView: NMFMapView, regionDidChangeAnimated animated: Bool, byReason reason: Int) {
        let cameraPosition = mapView.cameraPosition
        
        
        NSLog("대상 지점 위도: %f, 대상 지점 경도: %f, 줌 레벨: %f, 기울임 각도: %f, 헤딩 각도: %f ",
               cameraPosition.target.lat, cameraPosition.target.lng,
               cameraPosition.zoom, cameraPosition.tilt, cameraPosition.heading);
    }
}

extension ViewController {
    func move(at coordinate: CLLocationCoordinate2D?) {
        guard let coordinate = coordinate else {
            return
        }
        
        print("move = \(coordinate)")
        
        let latitude = coordinate.latitude
        let longitude = coordinate.longitude
        
        print(tmapAPI.getAddrFromGpsPoint(latitude, lng: longitude))
//        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 14.0)
//        mapView.camera = camera
//
//        myMarker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
//        myMarker.title = "My Position"
//        myMarker.snippet = "Known"
//        myMarker.map = mapView
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let firstLocation = locations.first else {
            return
        }
        
        move(at: firstLocation.coordinate)
    }
}
