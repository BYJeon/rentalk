//
//  DriverReq.swift
//  rentalk
//
//  Created by Yuna Daddy on 05/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import Foundation
import SwiftyJSON

class DriverReq{
    var api_token: String = ""
    var start_point: Address = Address()
    var end_point: Address = Address()
    var way_point: Address = Address()
    
    var number_info: String = ""
    var request_gb: String = "0"
    var pay_gb: String = "0"
    var center_gb: String = "0"
    var add_req: String = ""
    var emp_id: String = ""
    var expected_dist: String = ""
    var expected_time: String = ""
    
    var caution : String = ""
    var insr_nm : String = ""
    var insr_no : String = ""
    var path_img : String = ""
    var add_req_car: String = "" //추가 요청 사항
    
    var is_has_mission = false
    var is_stick_gear = false
    var is_one_license = false
    var is_has_repairShop = true
    
    struct Address{
        var address: String = ""
        var la: String  = ""
        var lo: String  = ""
        var tel_no: String = ""
        var nm: String = ""
    }
    
    init() {
    }
    
    func addPoint(addr: PathItem, type : PathType){
        if type == PathType.TARGET{
            start_point.address = addr.getAddress()
            start_point.la = addr.getLatInfo().description
            start_point.lo = addr.getLngInfo().description
        }else if type == PathType.SOURCE{
            end_point.address = addr.getAddress()
            end_point.la = addr.getLatInfo().description
            end_point.lo = addr.getLngInfo().description
            end_point.tel_no = number_info
        }else{
            way_point.address = addr.getAddress()
            way_point.la = addr.getLatInfo().description
            way_point.lo = addr.getLngInfo().description
            way_point.tel_no = number_info
            end_point.tel_no = ""
        }
    }
    
    func toAddrJSON(info: Address) -> JSON{
        
        var add = JSON()
        add["address"].stringValue = info.address
        add["la"].stringValue = info.la
        add["lo"].stringValue = info.lo
        add["tel_no"].stringValue = info.tel_no
        add["nm"].stringValue = info.nm
        
        return add
    }
    
    func toMissionJSON() -> JSON{
        var mission = JSON()
        mission["mission_ids"].arrayObject = []
        if is_has_mission{
            mission["mission_ids"].arrayObject = ["MI0003"]
        }
        
        return mission
    }
    
    func toCautionJSON() -> JSON{
        var caution = JSON()
        if is_stick_gear && is_one_license {
            caution.arrayObject = ["1, 2"]
        }else if is_stick_gear && !is_one_license {
            caution.arrayObject = ["2"]
        }else if !is_stick_gear && is_one_license{
            caution.arrayObject = ["1"]
        }else{
            caution.arrayObject = []
        }
        
        return caution
    }
    
    func toJSON() -> JSON {
        let pathManager = PathInfoManager.sharedInstance
        let infoManager = InfoManager.getInstance()
        var json = JSON()
        
        let isCompanyStart = pathManager.isCompanyStart()
        let wayAddr = way_point.address
        
        if isCompanyStart && wayAddr.count == 0 {
            request_gb = "0"
        }else if isCompanyStart && wayAddr.count > 0 {
            request_gb = "1"
        }else if !isCompanyStart && wayAddr.count == 0 {
            request_gb = "2"
        }else if !isCompanyStart && wayAddr.count > 0 {
            request_gb = "3"
        }
        
        if way_point.address.count != 0 {
            json = [
                "api_token" : infoManager.getApiToken(),
                "start_point" : toAddrJSON(info: start_point),
                "waypoint" : toAddrJSON(info: way_point),
                "end_point" : toAddrJSON(info: end_point),
                "request_gb" : request_gb,
                "pay_gb" : pay_gb,
                "center_gb" : center_gb,
                "add_req" : add_req,
                "emp_id"  : infoManager.getAppID(),
                "expected_dist" : expected_dist,
                "expected_time" : expected_time,
                "mission" : toMissionJSON(),
                "caution" : toCautionJSON(),
                "insr_nm" : insr_nm,
                "insr_no" : insr_no,
                "path_img" : path_img,
                "add_req_car" : add_req_car
            ]
        }else{
            json = [
                "api_token" : infoManager.getApiToken(),
                "start_point" : toAddrJSON(info: start_point),
                "end_point" : toAddrJSON(info: end_point),
                "request_gb" : request_gb,
                "pay_gb" : pay_gb,
                "center_gb" : center_gb,
                "add_req" : add_req,
                "emp_id"  : infoManager.getAppID(),
                "expected_dist" : expected_dist,
                "expected_time" : expected_time,
                "mission" : toMissionJSON(),
                "caution" : toCautionJSON(),
                "insr_nm" : insr_nm,
                "insr_no" : insr_no,
                "path_img" : path_img,
                "add_req_car" : add_req_car
            ]
        }
        
        return json
    }
}
