//
//  RegisterTableViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 02/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Mixpanel

class RegisterTableViewController: BaseTableViewController {
    //이미지 등록
    var imagePicker = UIImagePickerController()
    @IBOutlet var licenseBtnImg: UIButton!
    
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var secureTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var companyPhoneTextField: UITextField!
    @IBOutlet var secureTimeLabel: UILabel!
    
    @IBOutlet var serviceAgreeCheck: CheckBox!
    @IBOutlet var privacyAgreeCheck: CheckBox!
    @IBOutlet var identityAgreeCheck: CheckBox!
    
    //timer
    private var mTimer : Timer?
    private var remainTime = 180
    
    //이용약관
    @IBOutlet var serviceAgreeLabel: UILabel!
    @IBOutlet var privacyAgreeLabel: UILabel!
    @IBOutlet var identityAgreeLabel: UILabel!
    
    private var isProfileImgDone = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let serviceGesture = UITapGestureRecognizer(target: self, action: #selector(self.serviceAgrddDidClicked(_:)))
        serviceAgreeLabel.isUserInteractionEnabled = true
        serviceAgreeLabel.addGestureRecognizer(serviceGesture)
        
        let privacyGesture = UITapGestureRecognizer(target: self, action: #selector(self.privacyAgrddDidClicked(_:)))
        privacyAgreeLabel.isUserInteractionEnabled = true
        privacyAgreeLabel.addGestureRecognizer(privacyGesture)
        
        let identityGesture = UITapGestureRecognizer(target: self, action: #selector(self.identityAgrddDidClicked(_:)))
        identityAgreeLabel.isUserInteractionEnabled = true
        identityAgreeLabel.addGestureRecognizer(identityGesture)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 8
    }
    
    //이용약관
    @objc func serviceAgrddDidClicked(_ sender: UIGestureRecognizer){
        openUrl(url: "https://www.rentalk.co.kr/storage/rentalk_service_clause.html")
    }
    
    @objc func privacyAgrddDidClicked(_ sender: UIGestureRecognizer){
        openUrl(url: "https://www.rentalk.co.kr/storage/personal_data.html")
    }
    
    @objc func identityAgrddDidClicked(_ sender: UIGestureRecognizer){
        openUrl(url: "https://www.rentalk.co.kr/storage/location_service_clause.html")
    }
    
    private func openUrl(url: String){
        guard let url = URL(string: url), UIApplication.shared.canOpenURL(url) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func registerBtnClicked(_ sender: Any) {
        print("Register Clicked")
        
        if !serviceAgreeCheck.isChecked || !privacyAgreeCheck.isChecked || !identityAgreeCheck.isChecked{
            if parentDelegate != nil {
                parentDelegate?.showPopupViewFromDelegate("약관 동의", "모든 약관에 동의해 주세요.")
            }
            return
        }
        
        if !isProfileImgDone {
            if parentDelegate != nil {
                parentDelegate?.showPopupViewFromDelegate("이미지 등록", "사업자 등록증 사진을 등록해주세요.")
            }
            return
        }
        
        #if RELEASE
        Mixpanel.mainInstance().track(event: "PAGE_LOAD",
                                      properties:["CONTENT": "SIGNUP", "STEP": "0"])
        #endif
        
        request(API_USER_REGIST)
        
        if parentDelegate != nil {
            parentDelegate?.childViewRequestToServer?()
        }
    }
    
    //운전면허 등록
    @IBAction func licenseBtnClicked(_ sender: Any) {
        self.openCamera()
    }
    
    @IBAction func secureBtnClicked(_ sender: Any) {
        request(API_USER_REGIST_REQ_NUM)
        
        remainTime = 180
        secureTimeLabel.isHidden = false
        if let timer = mTimer {
            //timer 객체가 nil 이 아닌경우에는 invalid 상태에만 시작한다
            if !timer.isValid {
                /** 1초마다 timerCallback함수를 호출하는 타이머 */
                mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
            }
        }else{
            //timer 객체가 nil 인 경우에 객체를 생성하고 타이머를 시작한다
            /** 1초마다 timerCallback함수를 호출하는 타이머 */
            mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
        }
    }
    
    // MARK:- camera picker
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: {
                action in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //타이머가 호출하는 콜백함수
    @objc func timerCallback(){
        if remainTime > 0 {
            remainTime -= 1
            secureTimeLabel.text = String(format: "%02d:%02d", remainTime/60, remainTime%60)
        }else{
            secureTimeLabel.isHidden = true
            mTimer?.invalidate()
        }
        
    }
    
    //MARK: - network
    override func request(_ name: String) {
        switch name {
        case API_USER_REGIST:
            let nm : String = nameTextField.trimmingCharacters()
            let phone : String = phoneTextField.trimmingCharacters()
            let secure : String = secureTextField.trimmingCharacters()
            let token : String  = InfoManager.getInstance().getToken()
            
            let params: Parameters = [
                "nm" : nm
                ,"emp_id" : phone
                ,"auth_no" : secure
                ,"fcm_token" : token
            ]
            
            if let setImage = licenseBtnImg.image(for: .normal) {
                var data = setImage.pngData()
                data = setImage.jpegData(compressionQuality: 1.0)
                _ = NetworkManager.sharedInstance.requestMultipartFormdata(name, params, [data!], target: self)
            }
            break
        case API_USER_REGIST_REQ_NUM:
            let phone : String = phoneTextField.trimmingCharacters()
            let params: Parameters = ["tel_no" : phone]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.post, target: self)
            break;
            
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        if parentDelegate != nil {
            parentDelegate?.childViewRequestComplete?()
        }
        
        switch requestID {
        case API_USER_REGIST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                #if RELEASE
                Mixpanel.mainInstance().track(event: "SIGN_UP",
                                              properties:["METHOD": "MAIN"])
                #endif
                
                if parentDelegate != nil {
                    parentDelegate?.showPopupViewFromDelegate("회원가입", "회원 가입이 완료 되었습니다.")
                }
                
            }else{
                if parentDelegate != nil {
                    parentDelegate?.showPopupViewFromDelegate("회원가입 오류", "관리자에게 문의해 주세요.")
                }
            }
            break
        case API_USER_REGIST_REQ_NUM:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                if parentDelegate != nil {
                    parentDelegate?.showPopupViewFromDelegate("인증번호 요청 완료", "인증번호를 확인해 주세요.")
                }
                
//                let dialog = UIAlertController(title: "인증번호 요청 완료", message: "인증번호를 확인해 주세요", preferredStyle: .alert)
//                let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
//                dialog.addAction(action)
//                self.present(dialog, animated: true, completion: nil)
            }
            break;
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        let json = JSON(data!)
        
        if parentDelegate != nil {
            parentDelegate?.showPopupViewFromDelegate("로그인 오류", "오류 번호 : " + json["success"].stringValue)
        }
        
//        let dialog = UIAlertController(title: "로그인 오류", message: "오류 번호 : " + json["success"].stringValue, preferredStyle: .alert)
//        let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
//        dialog.addAction(action)
//        self.present(dialog, animated: true, completion: nil)
        
        secureTimeLabel.isHidden = true
    }
}

extension RegisterTableViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImage = info[.editedImage] as? UIImage {
            licenseBtnImg.setImage(editedImage, for: .normal)
            licenseBtnImg.imageView?.contentMode = .scaleAspectFit
            isProfileImgDone = true
        }
//        else if let originalImage = info[.originalImage] as? UIImage {
//            licenseBtnImg.image = originalImage
//        } else{
//            print("Something went wrong")
//        }
        // use the image
        dismiss(animated: true, completion: {})
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        isProfileImgDone = false
    }
}

extension RegisterTableViewController: UINavigationControllerDelegate {
    
}
