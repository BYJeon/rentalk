//
//  AddrCustomCell.swift
//  rentalk
//
//  Created by Yuna Daddy on 15/07/2019.
//  Copyright © 2019 avara. All rights reserved.
//

import UIKit

class AddrCustomCell: UITableViewCell {

    @IBOutlet var contentLabel: UILabel!
    @IBOutlet var addrLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
