//
//  HistoryViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 05/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit

class HistoryViewController: BaseSwipeViewController, UITextFieldDelegate {

    @IBOutlet var numTextField: UITextField!
    private var childTableView: HistoryTableViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        numTextField.keyboardType = .numbersAndPunctuation
        
        numTextField.delegate = self
        childTableView = self.children[0] as? HistoryTableViewController
        childTableView.parentDelegate = self
    }

    @IBAction func backDidClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func allViewDidClicked(_ sender: Any) {
        numTextField.text = ""
        childTableView.reloadReuqestInfo("")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        childTableView.reloadReuqestInfo(numTextField.text!)
        return true
    }
    
    ///Parent Delegate
    override func showPopupViewFromDelegate(_ title: String, _ content: String) {
    }
    
    override func moveToPagePresentFromDelegate(_ id: String) {
        if mainOperationDelegate != nil {
            mainOperationDelegate?.moveToPage(id: id)
        }
    }
    
    override func childViewRequestComplete() {
        showLoadingProgressBar(isShow: false)
    }
}
