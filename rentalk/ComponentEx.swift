//
//  StringEx.swift
//  rentalk
//
//  Created by Yuna Daddy on 07/08/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import Foundation

extension String {
    var lastPathComponent: String {
        
        get {
            return (self as NSString).lastPathComponent
        }
    }
    
    func getCurrencyString() -> String {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .decimal
        currencyFormatter.groupingSize = 3
        currencyFormatter.alwaysShowsDecimalSeparator = false
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.locale = NSLocale.current
        
        if let number = Double(self) {
            //            println(">>> number = \(number)")
            let transNumber = NSNumber(value:number)
            //            println(">>> number = \(transNumber)")
            
            let priceString: String = currencyFormatter.string(for: transNumber)!
            //            println(">>> priceString = \(priceString)")
            return priceString
        }
        else {
            return self
        }
    }
}

extension UITextField {
    func setTextFieldPaddingAndBorder(_ amount: CGFloat) { //왼쪽에 여백 주기
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
        self.layer.cornerRadius = 3
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func trimmingCharacters() -> String {
        if let textField = self.text {
            return textField.trimmingCharacters(in: .whitespaces)
        }
        return ""
    }
}

extension UIColor {
    func colorWithRGBHex(hex: Int, alpha: Float = 1.0) -> UIColor {
        let r = Float((hex >> 16) & 0xFF)
        let g = Float((hex >> 8) & 0xFF)
        let b = Float((hex) & 0xFF)
        
        return UIColor(red: CGFloat(r / 255.0), green: CGFloat(g / 255.0), blue:CGFloat(b / 255.0), alpha: CGFloat(alpha))
    }
}
