//
//  RequestStatusViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 05/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit

class RequestStatusDetailViewController: BaseSwipeViewController {
    //popupview
    @IBOutlet var dimView: UIView!
    @IBOutlet var popupView: UIView!
    @IBOutlet var popupTitleLabel: UILabel!
    @IBOutlet var popupContentLabel: UITextView!
    
    @IBOutlet var screenShotImg: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showLoadingProgressBar(isShow: true)
        
        let childView = self.children[0] as! BaseTableViewController
        childView.parentDelegate = self
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dimDidClicked(_:)))
        self.dimView.addGestureRecognizer(gesture)
    }
    
    @IBAction func backDidClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //DimView Clicked
    @objc func dimDidClicked(_ sender: UIGestureRecognizer){
        hidePopupView()
    }
    
    private func showPopupView(_ title: String, _ content: String){
        popupTitleLabel.text = title
        popupContentLabel.text = content
        
        popupView.isHidden = false
        dimView.isHidden = false
    }
    private func hidePopupView(){
        popupView.isHidden = true
        dimView.isHidden = true
    }
    
    ///Parent Delegate
    override func showPopupViewFromDelegate(_ title: String, _ content: String) {
        showPopupView(title, content)
    }
    
    override func moveToPagePresentFromDelegate(_ id: String) {
        if mainOperationDelegate != nil {
            mainOperationDelegate?.moveToPageByPresent(id: id)
        }
    }
    
    override func moveToPagePresentFromDelegate(_ id: String, title: String, urlInfo: String) {
        if mainOperationDelegate != nil {
            mainOperationDelegate?.moveToPageByPresent?(id: id, title: title, url: urlInfo)
        }
    }
    func moveToPagePresentFromDelegate(_ id: String, reqID: String) {
        if mainOperationDelegate != nil {
            mainOperationDelegate?.moveToPageByPresent?(id: id, reqID: reqID)
        }
    }
    
    func takeScreenShot() {
        var img: UIImage = self.view.takeScreenshot(600)
        screenShotImg.image = img
        let items = [img]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
    }
}
