//
//  InfoManager.swift
//  rentalk
//
//  Created by Yuna Daddy on 07/08/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit

enum MenuType {
    case NORMAL
    case ADDRCONFIRM
    case DRIVERPICK
    case SET_REQUEST_PATH
}

class InfoManager: NSObject {
    private var fcmToken : String? = nil
    private var apiToken : String? = nil
    private var appID : String? = nil
    
    var reqID : String = "" //요청 ID
    var expect_amt : String = "0" //총 소요 금액
    var dc_amt : String = "0" //할인 금액
    
    var start_point_address : String = ""
    var way_point_address : String = ""
    var end_point_address : String = ""
    
    var driverInfo : DriverInfo? = nil
    var requestInfo : RequestInfo? = nil
    
    var driverImgFilePath = ""
    var companyTypeGb = ""
    
    private var menuType : MenuType = MenuType.NORMAL
    private let apiTokenStr  = "apiTokenStr"
    private let appIDStr     = "appIDStr"
    private let companyTypeStr  = "companyTypeStr"
    
    /// Save the single instance
    static private var instance : InfoManager {
        return sharedInstance
    }
    
    private static let sharedInstance = InfoManager()
    
    /**
     Singleton pattern method
     
     - returns: FavoriteManager single instance
     */
    static func getInstance() -> InfoManager {
        return instance
    }
    
    private override init() {
        super.init()
        
        loadSettingsData()
    }
    
    func storeData(_ key:String, data:String){
        let sharedPref = UserDefaults.standard
        sharedPref.setValue(data, forKey: key)
        sharedPref.synchronize()
    }
    
    private func retrieveData(_ key:String) -> String{
        let sharedPref = UserDefaults.standard
        let result =  sharedPref.string(forKey: key)
        if result == nil { return "" }
        return result!
    }
    
    private func loadSettingsData(){
        self.apiToken = retrieveData(apiTokenStr)
        self.appID   = retrieveData(appIDStr)
        self.companyTypeGb = retrieveData(companyTypeStr)
        if self.companyTypeGb.count == 0 {
           self.companyTypeGb = "1"
        }
    }

    func setDriverInfo(driverID: String, driver_nm: String, dist_gap: String, missions: [String], expected_amt: String, profile_photo: String, eval_score: String,
                       start_point_la: String, start_point_lo: String, end_point_la: String, end_point_lo: String, waypoint_la: String, waypoint_lo: String){
        driverInfo = DriverInfo(driverID: driverID, driver_nm: driver_nm,dist_gap: dist_gap,missions: missions,expected_amt: expected_amt,profile_photo: profile_photo,eval_score: eval_score,start_point_la: start_point_la,start_point_lo: start_point_lo,end_point_la: end_point_la,end_point_lo: end_point_lo,waypoint_la: waypoint_la,waypoint_lo: waypoint_lo)
    }
    func setMenuType(type : MenuType){ self.menuType = type }
    func getMenuType() -> MenuType{ return self.menuType }
    
    func setApiToken(token : String ){
        self.apiToken = token
        storeData(apiTokenStr, data: self.apiToken!)
    }
    func getApiToken() -> String { return self.apiToken! }
    
    func setAppID(id: String){
        self.appID = id
        storeData(appIDStr, data: self.appID!)
    }
    
    func getAppID() -> String{
        return self.appID!
    }
    
    func setCompanyType(type : String ){
        self.companyTypeGb = type
        storeData(companyTypeStr, data: self.companyTypeGb)
    }
    
    func setToken(token : String ){ self.fcmToken = token }
    func getToken() -> String { return self.fcmToken! }
}
