//
//  BaseMapView.swift
//  rentalk
//
//  Created by Yuna Daddy on 06/08/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import NMapsMap

class BaseMapView: UIView, NMFMapViewDelegate{
    internal var mapView : NMFMapView? = nil
    internal let tmapAPI = TmapAPI()
    internal var curPosition : NMFCameraPosition? = nil
    internal var pathDele : PathManagerDelegate? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let button = self.viewWithTag(102) as! UIButton
        button.setTitleColor(UIColor.colorWithRGBHex(hex: 0x78849E), for: .normal)
        button.backgroundColor = UIColor.colorWithRGBHex(hex: 0xF5F7F8)
    }
    
    private func commonInit() {
        for view in self.subviews {
            view.removeFromSuperview()
        }
        
        //custom initialization
       mapView = NMFMapView(frame: self.bounds)
       insertSubview(mapView!, at: 0)
       
       mapView?.delegate = self
        
       let image: UIImage = UIImage(named: "ico_map_pin")!
       let imageView = UIImageView(image: image)
       imageView.center = CGPoint(x: self.frame.size.width  / 2,
                                   y: self.frame.size.height / 2);
       insertSubview(imageView, at: 1)
        
       let button = UIButton();
       button.tag = 102
       button.setTitle("설정 완료", for: .normal)
       button.titleLabel?.font = .systemFont(ofSize: 20)
       button.setTitleColor(UIColor.white, for: .normal)
       button.frame = CGRect(x: 15, y: self.frame.size.height - 60, width: self.frame.size.width - 30, height: 60)
       button.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486)
       button.addTarget(self, action: #selector(self.pressButton(_:)), for: .touchUpInside)
       insertSubview(button, at: 2)
       
       let pathInfoManager = PathInfoManager.sharedInstance
       if pathInfoManager.myLocationInfo == nil { return }
       let latitude = pathInfoManager.myLocationInfo!.coordinate.latitude
       let longitude = pathInfoManager.myLocationInfo!.coordinate.longitude

       let cameraUpdate = NMFCameraUpdate(scrollTo: NMGLatLng(lat: latitude, lng: longitude))
       mapView!.moveCamera(cameraUpdate)

       let locationOverlay = mapView?.locationOverlay
       locationOverlay?.hidden = false
       locationOverlay?.location = NMGLatLng(lat: latitude, lng: longitude)
    }
    
    override func updateConstraints() {
        //set subview constraints here
        super.updateConstraints()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        commonInit()
    }
    
    func setInfoDelegate(_ dele : PathManagerDelegate){
        self.pathDele = dele
    }
    
    func mapView(_ mapView: NMFMapView, regionDidChangeAnimated animated: Bool, byReason reason: Int) {
        curPosition = mapView.cameraPosition
        NSLog("대상 지점 위도: %f, 대상 지점 경도: %f, 줌 레벨: %f, 기울임 각도: %f, 헤딩 각도: %f ",
              curPosition!.target.lat, curPosition!.target.lng,
              curPosition!.zoom, curPosition!.tilt, curPosition!.heading);
        
    }
    
    func mapViewIdle(_ mapView: NMFMapView) {
        let button = self.viewWithTag(102) as! UIButton
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486)
        
        if pathDele != nil {
            pathDele?.displayAddrByMap?(lat: curPosition!.target.lat, lng: curPosition!.target.lng)
        }
    }
    //The target function
    @objc func pressButton(_ sender: UIButton){ //<- needs `@objc`
        if pathDele != nil{
            if curPosition == nil{
                curPosition = mapView?.cameraPosition
            }
            pathDele?.selectedAddrByMap?(lat: curPosition!.target.lat, lng: curPosition!.target.lng)
        }
    }
}
