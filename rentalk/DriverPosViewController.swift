//
//  DriverPosViewController.swift
//  rentalk
//
//  Created by Yuna on 08/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import NMapsMap
import Alamofire
import SwiftyJSON

class DriverPosViewController: BaseMapViewController {
    private let targetMarker = NMFMarker()
    private let sourceMarker = NMFMarker()
    private let viaMarker = NMFMarker()
    private let pathInfoManager = PathInfoManager.sharedInstance
    
    @IBOutlet var topGradientView: UIView!
    
    private var isMarkerLoaed = false
    //timer
    private var mTimer : Timer?
    private let infoManager = InfoManager.getInstance()
    
    private var driverPosLat : Double = 0
    private var driverPosLng : Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showLoadingProgressBar(isShow: true)
        
        request(API_DRIVER_RIDING_LOCATION_REQUEST)
        
        if let timer = mTimer {
            //timer 객체가 nil 이 아닌경우에는 invalid 상태에만 시작한다
            if !timer.isValid {
                mTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
            }
        }else{
            mTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        topGradientView.bottomAlphGradient(startPos: 0.0, endPos: 0.75, viewWidth: self.view.frame.width, alph: 0.8)
    }
    
    @IBAction func moveCurPosDidClicked(_ sender: Any) {
        isMarkerLoaed = false
        request(API_DRIVER_RIDING_LOCATION_REQUEST)
    }
    
    @IBAction func closeDidClicked(_ sender: Any) {
        if mTimer != nil && mTimer!.isValid {
            mTimer?.invalidate()
        }
        pathInfoManager.clearPathItem()
        dismiss(animated: true, completion: nil)
    }
    
    //타이머가 호출하는 콜백함수
    @objc func timerCallback(){
        request(API_DRIVER_RIDING_LOCATION_REQUEST)
    }
    
    // 지도에 출발/도착/경유지 표시
    func setRequestPathToMap() {
        targetMarker.mapView = nil
        sourceMarker.mapView = nil
        viaMarker.mapView = nil
        
        let targetPath = pathInfoManager.getTargetInfo()
        let sourcePath = pathInfoManager.getSourceInfo()
        let viaPath = pathInfoManager.getRepariShopInfo()
        
        
        if targetPath == nil || sourcePath == nil {return}
        
        
        if targetPath != nil {
            setMarkerInMap(marker: targetMarker, lat: targetPath!.getLatInfo(), lng: targetPath!.getLngInfo(), title: "출발")
        }
        
        if sourcePath != nil {
            setMarkerInMap(marker: sourceMarker, lat: sourcePath!.getLatInfo(), lng: sourcePath!.getLngInfo(), title: "도착")
        }
        
        if viaPath != nil{
            setMarkerInMap(marker: viaMarker, lat: viaPath!.getLatInfo(), lng: viaPath!.getLngInfo(), title: "경유")
        }
        
        let southWestPos = pathInfoManager.getSouthWestPathInfo()
        let northEastPos = pathInfoManager.getNorthEastPathInfo()
        
        if southWestPos != nil && northEastPos != nil {
            let bounds = NMGLatLngBounds(southWestLat: southWestPos!.getLatInfo(), southWestLng: southWestPos!.getLngInfo(), northEastLat: northEastPos!.getLatInfo(), northEastLng: northEastPos!.getLngInfo())
            
            let cameraUpdate = NMFCameraUpdate(fit: bounds, padding: 100)
            
            cameraUpdate.animation = .easeIn
            cameraUpdate.animationDuration = 1
            mapView!.moveCamera(cameraUpdate)
            isMarkerLoaed = true
        }
    }
    
    func setMarkerInMap(marker: NMFMarker, lat: Double, lng: Double, title: String){
        marker.position = NMGLatLng(lat: lat, lng: lng)
        let markerImg = title == "출발" ? "ico_map_pincircle_g" : "ico_map_pincircle_y"
        marker.iconImage = NMFOverlayImage(name: markerImg)
        marker.mapView = mapView
        
        let infoWindow = NMFInfoWindow()
        let customInfo = CustomInfoWindow(title: title)
        infoWindow.dataSource = customInfo
        infoWindow.open(with: marker)
    }
    
    //MARK: - network
    override func request(_ name: String) {
        switch name {
        case API_DRIVER_RIDING_LOCATION_REQUEST:
            let extraParam : String = "/" + urlInfo + "?api_token="+infoManager.getApiToken()
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        showLoadingProgressBar(isShow: false)
        switch requestID {
        case API_DRIVER_RIDING_LOCATION_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                
                let start_point_la = Double(json["start_point"]["la"].stringValue)
                let start_point_lo = Double(json["start_point"]["lo"].stringValue)
                
                let end_point_la = Double(json["end_point"]["la"].stringValue)
                let end_point_lo = Double(json["end_point"]["lo"].stringValue)
                
                let wayPathInfo = json["waypoint"]["la"].stringValue
                
                let way_point_la = Double(json["waypoint"]["la"].stringValue)
                let way_point_lo = Double(json["waypoint"]["lo"].stringValue)
                
                driverPosLat = Double(json["current_location"]["la"].stringValue)!
                driverPosLng = Double(json["current_location"]["lo"].stringValue)!
                
                
                let locationOverlay = mapView?.locationOverlay
                locationOverlay?.hidden = false
                locationOverlay?.location = NMGLatLng(lat: driverPosLat, lng: driverPosLng)
                
                
                let start_address : String = tmapAPI.getAddrFromGpsPoint(start_point_la!, lng: start_point_lo!)
                let end_address : String = tmapAPI.getAddrFromGpsPoint(end_point_la!, lng: end_point_lo!)
                
                let targetPathItem = PathItem(lat: start_point_la! , lng: start_point_lo!, address: start_address)
                let sourcePathItem = PathItem(lat: end_point_la! , lng: end_point_lo!, address: end_address)
                
                pathInfoManager.setPathInfo(item: targetPathItem, pathType: .TARGET, isViaMode: false)
                pathInfoManager.setPathInfo(item: sourcePathItem, pathType: .SOURCE, isViaMode: false)
                
                if wayPathInfo.count != 0 {
                    let way_address : String = tmapAPI.getAddrFromGpsPoint(way_point_la!, lng: way_point_lo!)
                    let wayPathItem = PathItem(lat: way_point_la! , lng: way_point_lo!, address: way_address)
                    pathInfoManager.setPathInfo(item: wayPathItem, pathType: .REPAIR_SHOP, isViaMode: false)
                }
                
                if !isMarkerLoaed{
                    setRequestPathToMap()
                }

            }
            break
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        showLoadingProgressBar(isShow: false)
        //let json = JSON(data!)
        
        let dialog = UIAlertController(title: "오류", message: "관리자에게 문의하세요.", preferredStyle: .alert)
        let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
        dialog.addAction(action)
        self.present(dialog, animated: true, completion: nil)
    }
}
