//
//  MoreTableViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 09/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit

class MoreTableViewController: BaseTableViewController {

    @IBOutlet var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            self.versionLabel.text = version
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 6
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        print(indexPath.row)
        if indexPath.row == 1 {
            if parentDelegate != nil {
                parentDelegate?.moveToPagePresentFromDelegate?("WebViewController", title: "이용약관", urlInfo: "https://www.rentalk.co.kr/storage/rentalk_service_clause.html")
            }
        }else if indexPath.row == 2 {
            if parentDelegate != nil {
                parentDelegate?.moveToPagePresentFromDelegate?("WebViewController", title: "위치기반 서비스 이용약관", urlInfo: "https://www.rentalk.co.kr/storage/location_service_clause.html")
            }
        }else if indexPath.row == 3 {
            if parentDelegate != nil {
                parentDelegate?.moveToPagePresentFromDelegate?("WebViewController", title: "개인정보처리방침", urlInfo: "https://www.rentalk.co.kr/storage/personal_data.html")
            }
        }else if indexPath.row == 4 {
            if parentDelegate != nil {
                //parentDelegate?.moveToPagePresentFromDelegate?("WebViewController", title: "오픈소스 라이선스", urlInfo: "")
            }
        }
    }
}
