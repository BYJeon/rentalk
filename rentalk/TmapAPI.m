//
//  NSObject+TmapAPI.m
//  rentalk
//
//  Created by Yuna Daddy on 06/08/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

#import "TmapAPI.h"

@implementation TmapAPI

-(id)init{
    self = [super init];
    [self createMapView];
    return self;
}

-(NSArray*)getROIList:(NSString*) searchWord{
    NSLog(@"Objective-C Class");
    
    TMapPathData* path = [[[TMapPathData alloc] init] autorelease];
    
    //통합검색
    NSArray* result = [path requestFindAllPOI:searchWord];
    //    int cnt = 0;
    //    int idx = 0;
    //    for(TMapPOIItem* poi in result)
    //    {
    //        if (cnt++ == 0) {
    //            [_mapView setCenterPoint:[poi getPOIPoint]];
    //        }
    //
    //        [_mapView addCustomObject:poi ID:[NSString stringWithFormat:@"poi%d", idx++]];
    //        NSLog(@"poi:%@", poi);
    //    }
    
    return result;
}

- (void)createMapView {
    _mapView = [[TMapView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [_mapView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [_mapView setSKTMapApiKey:TMAP_APPKEY];     // 발급 받은 apiKey 설정
    //[self.mapContainerView addSubview:_mapView];
}

-(NSString*)getAddrFromGpsPoint:(double)lat lng:(double)lng{
    TMapPathData* path = [[[TMapPathData alloc] init] autorelease];
    TMapPoint* pos = [[TMapPoint alloc] initWithLon:lng Lat:lat];
    
    return [path convertGpsToAddressAt:pos];
}

-(NSDictionary*)getPathDistanceInfo:(double)lat lng:(double)lng slat:(double)slat slng:(double)slng{
    TMapPathData* path = [[[TMapPathData alloc] init] autorelease];
    
    TMapPoint* start_point = [[TMapPoint alloc] initWithLon:lng Lat:lat];
    TMapPoint* end_point = [[TMapPoint alloc] initWithLon:slng Lat:slat];
    
    NSDate *date = [NSDate date];
    
    return [path findTimeMachineCarPathWithStartPoint:start_point endPoint:end_point isStartTime:true time:date wayPoints:nil];
}

-(NSDictionary*)getPathDistanceInfoWithVia:(double)lat lng:(double)lng
                                      slat:(double)slat slng:(double)slng
                                      vialat:(double)vialat vialng:(double)vialng{
    TMapPathData* path = [[[TMapPathData alloc] init] autorelease];
    
    TMapPoint* start_point = [[TMapPoint alloc] initWithLon:lng Lat:lat];
    TMapPoint* end_point = [[TMapPoint alloc] initWithLon:slng Lat:slat];
    TMapPoint* way_point = [[TMapPoint alloc] initWithLon:vialng Lat:vialat];
    
    NSArray* arr = [NSArray arrayWithObjects:way_point, nil];
    NSDate *date = [NSDate date];
    
    return [path findTimeMachineCarPathWithStartPoint:start_point endPoint:end_point isStartTime:true time:date wayPoints:arr];
}

@end
