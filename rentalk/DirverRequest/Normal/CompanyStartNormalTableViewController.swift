//
//  CompanyStartNormalTableViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 09/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Mixpanel

class CompanyStartNormalTableViewController: BaseTableViewController, UITextFieldDelegate {

    @IBOutlet var extraRequestCheck: CheckBox!
    @IBOutlet var stickGearCheck: CheckBox!
    @IBOutlet var licenseCheck: CheckBox!
    @IBOutlet var payCashCheck: CheckBox!
    
    
    @IBOutlet var numberTextField: UITextField!
    @IBOutlet var extraTextField: UITextField!
    
    @IBOutlet var pathInfoView: UIView!
    @IBOutlet var pathViaView: UIView!
    
    //경유지가 없는 경로
    @IBOutlet var targetPath: UITextView!
    @IBOutlet var sourcePath: UITextView!
    
    //경유지가 있는 경로
    @IBOutlet var targetViaPath: UITextView!
    @IBOutlet var viaPath: UITextView!
    @IBOutlet var sourceViaPath: UITextView!
    
    @IBOutlet var contactInfoLabel: UILabel!
    
    private let tmapAPI = TmapAPI()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundView = UIImageView(image: UIImage(named: "bg_diagonal"))
        payCashCheck.isChecked = true
        payCashCheck.isUserInteractionEnabled = false

        numberTextField.delegate = self
        numberTextField.keyboardType = .numbersAndPunctuation

        extraTextField.isUserInteractionEnabled = false

        extraTextField.delegate = self

        //경유지가 존재할 경우
        if pathInfoManager.isHasRepairShopInfo() {
            pathViaView.isHidden = false
            pathInfoView.isHidden = true

            targetViaPath.text = pathInfoManager.getTargetInfo()!.getAddress()
            sourceViaPath.text   = pathInfoManager.getSourceInfo()!.getAddress()
            viaPath.text       = pathInfoManager.getRepariShopInfo()!.getAddress()

            contactInfoLabel.text = "경유지(임차인) 연락처 정보"
            numberTextField.placeholder = "경유지(임차인) 전화번호 입력"
        }else{
            pathViaView.isHidden = true
            pathInfoView.isHidden = false

            targetPath.text = pathInfoManager.getTargetInfo()!.getAddress()
            sourcePath.text   = pathInfoManager.getSourceInfo()!.getAddress()

            contactInfoLabel.text = "도착지 연락처 정보"
            numberTextField.placeholder = "도착지 전화번호 입력"
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 7
    }
    
    // Controller Event
    @IBAction func closeDidClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func extraDidClicked(_ sender: Any) {
        extraTextField.isUserInteractionEnabled = !extraRequestCheck.isChecked
        
        if extraRequestCheck.isChecked{
            self.view.endEditing(true)
        }
    }
    
    @IBAction func payCachDidClicked(_ sender: Any) {
        print("Pay Cash")
    }
    
    @IBAction func driverRequestDidClicked(_ sender: Any) {
        if numberTextField.text?.count == 0 {
            if parentDelegate != nil {
                parentDelegate?.showPopupViewFromDelegate("탁송요청 실패", "연락처 정보를 입력해 주세요.")
            }
            
            return;
        }
        request(API_DRIVER_REQUEST)
        
        #if RELEASE
        Mixpanel.mainInstance().track(event: "BEGIN_CHECKOUT",
                                      properties:["METHOD": "DIRECT"])
        #endif
        
        if parentDelegate != nil {
            parentDelegate?.childViewRequestToServer?()
        }
        
        //        if parentDelegate != nil {
        //            parentDelegate?.moveToPagePresentFromDelegate("DriverCallingViewController")
        //        }
    }
    
    
    //TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    //MARK: - network
    override func request(_ name: String) {
        switch name {
        case API_DRIVER_REQUEST:
            
            let driverReq = DriverReq()
            driverReq.number_info = numberTextField.text!
            
            let startPoint = pathInfoManager.getTargetInfo()!
            let endPoint = pathInfoManager.getSourceInfo()!
            var wayPoint : PathItem? = nil
            
            driverReq.addPoint(addr: startPoint, type: PathType.TARGET)
            driverReq.addPoint(addr: endPoint, type: PathType.SOURCE)
            if pathInfoManager.isHasRepairShopInfo() {
                wayPoint = pathInfoManager.getRepariShopInfo()
                driverReq.addPoint(addr: wayPoint!, type: PathType.REPAIR_SHOP)
                
                let distanceDic = tmapAPI.getPathDistanceInfoWith(via: startPoint.getLatInfo(), lng: startPoint.getLngInfo(), slat: endPoint.getLatInfo(), slng: endPoint.getLngInfo(), vialat: (wayPoint?.getLatInfo())!, vialng: (wayPoint?.getLngInfo())!)
                
                let json = JSON(distanceDic)
                
                driverReq.expected_dist = json["features"][0]["properties"]["totalDistance"].stringValue
                driverReq.expected_time = json["features"][0]["properties"]["totalTime"].stringValue
                
            }else{
                let distanceDic = tmapAPI.getPathDistanceInfo(startPoint.getLatInfo(), lng: startPoint.getLngInfo(), slat: endPoint.getLatInfo(), slng: endPoint.getLngInfo())
                
                let json = JSON(distanceDic)
                
                driverReq.expected_dist = json["features"][0]["properties"]["totalDistance"].stringValue
                driverReq.expected_time = json["features"][0]["properties"]["totalTime"].stringValue
            }
            
            //mission
            driverReq.add_req = extraTextField.text!
            
            //caution
            driverReq.is_stick_gear = stickGearCheck.isChecked
            driverReq.is_one_license = licenseCheck.isChecked
            
            driverReq.path_img = InfoManager.getInstance().driverImgFilePath
            
            let params: Parameters = driverReq.toJSON().dictionaryObject!
            print(params)
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.post, target: self)
            break
            
        default:
            break
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        if parentDelegate != nil {
            parentDelegate?.childViewRequestComplete?()
        }
        
        switch requestID {
        case API_DRIVER_REQUEST:
            
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                //InfoManager.getInstance().setApiToken(token: json["api_token"].stringValue)
                let infoManager = InfoManager.getInstance()
                infoManager.reqID = json["req_id"].stringValue
                infoManager.dc_amt = json["dc_amt"].stringValue
                infoManager.expect_amt = json["expect_amt"].stringValue
                
                infoManager.start_point_address = json["start_point_address"].stringValue
                infoManager.end_point_address = json["end_point_address"].stringValue
                infoManager.way_point_address = json["waypoint_address"].stringValue
                
                if parentDelegate != nil {
                    parentDelegate?.moveToPagePresentFromDelegate("DriverCallingViewController")
                }
            }
            break
            
        default:
            break
        }
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        //let json = JSON(data!)
        
        if parentDelegate != nil {
            parentDelegate?.childViewRequestComplete?()
            
        }
        
        if parentDelegate != nil {
            parentDelegate?.showPopupViewFromDelegate("탁송요청 오류", "관리자에 문의하세요.")
        }
        
        //let dialog = UIAlertController(title: "로그인 오류", message: "오류 번호 : " + json["success"].stringValue, preferredStyle: .alert)
        //let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
        //dialog.addAction(action)
        //self.present(dialog, animated: true, completion: nil)
        //secureTimeLabel.isHidden = true
        //showPopupView("로그인 오류", json["tel_no"][0].stringValue)
    }

}
