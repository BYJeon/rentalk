//
//  ContractViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 10/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AWSS3
import NYTPhotoViewer

class ContractViewController: BaseSwipeViewController {

    @IBOutlet var contractImg: UIImageView!
    @IBOutlet var contract2Img: UIImageView!
    
    private var downloadProgress : Float = 0
    @objc var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
    
    @objc lazy var transferUtility = {
        AWSS3TransferUtility.default()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(urlInfo)
        // Do any additional setup after loading the view.
        request(API_DRIVER_CONTRACT_REQUEST)
        showLoadingProgressBar(isShow: true)
        
        
        let gestureRequest = UITapGestureRecognizer(target: self, action: #selector(self.contractClicked(_:)))
        self.contractImg.addGestureRecognizer(gestureRequest)
        
        let gesture2Request = UITapGestureRecognizer(target: self, action: #selector(self.contract2Clicked(_:)))
        self.contract2Img.addGestureRecognizer(gesture2Request)
        
    }
    
    @IBAction func closeDidClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    // 계약서 조회 클릭
    @objc func contractClicked(_ sender: UIGestureRecognizer){
        let photo = ContractImage()
        photo.image = contractImg.image
        let photosViewController = NYTPhotosViewController(photos: [photo])
        photosViewController.overlayView?.rightBarButtonItem = nil
        getTopMostViewController()!.present(photosViewController, animated: true, completion: nil)
    }
    
    @objc func contract2Clicked(_ sender: UIGestureRecognizer){
        let photo = ContractImage()
        photo.image = contract2Img.image
        let photosViewController = NYTPhotosViewController(photos: [photo])
        photosViewController.overlayView?.rightBarButtonItem = nil
        getTopMostViewController()!.present(photosViewController, animated: true, completion: nil)
    }
    
    func getTopMostViewController() -> UIViewController? {
        var topMostViewController = UIApplication.shared.keyWindow?.rootViewController
        
        while let presentedViewController = topMostViewController?.presentedViewController {
            topMostViewController = presentedViewController
        }
        
        return topMostViewController
    }
    
    //MARK: - network
    override func request(_ name: String) {
        switch name {
        case API_DRIVER_CONTRACT_REQUEST: //계약서 조회
            let extraParam : String = "/" + urlInfo
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
        default:
            break
        }
    }
    
    
    private func downloadDriverContract(_ imgPath: String, index: Int){
        if imgPath.count < 2 { return }
        
        let expression = AWSS3TransferUtilityDownloadExpression()
        expression.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                if (self.downloadProgress < Float(progress.fractionCompleted)) {
                    self.downloadProgress = Float(progress.fractionCompleted)
                }
            })
        }
        
        self.completionHandler = { (task, location, data, error) -> Void in
            DispatchQueue.main.async(execute: {
                if let error = error {
                    NSLog("Failed with error: \(error)")
                    //self.statusLabel.text = "Failed"
                }
                else if(self.downloadProgress != 1.0) {
                    //self.statusLabel.text = "Failed"
                }
                else{
                    if index == 0 {
                        self.contractImg.image = UIImage(data: data!)
                    }else{ // Path 정보
                        self.contract2Img.image = UIImage(data: data!)
                    }
                }
            })
        }
        
        transferUtility.downloadData(
            forKey: imgPath,
            expression: expression,
            completionHandler: completionHandler).continueWith { (task) -> AnyObject? in
                if let error = task.error {
                    NSLog("Error: %@",error.localizedDescription);
                    DispatchQueue.main.async(execute: {
                        //self.statusLabel.text = "Failed"
                    })
                }
                
                if let _ = task.result {
                    DispatchQueue.main.async(execute: {
                        //self.statusLabel.text = "Downloading..."
                    })
                    NSLog("Download Starting!")
                    // Do something with uploadTask.
                }
                return nil;
        }
    }
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        showLoadingProgressBar(isShow: false)
        switch requestID {
        case API_DRIVER_CONTRACT_REQUEST:
            let json = JSON(data!)
            if json["success"].stringValue == "0000" {
                
                let contract1 = json["result_array"][0]["photo1"].stringValue
                let contract2 = json["result_array"][0]["photo2"].stringValue
                
                if contract1.count > 1 {
                    
                    let pre = contract1.index(contract1.startIndex, offsetBy: 0)
                    let prefix = String(contract1[pre])
                    
                    if prefix == "c"{
                        self.downloadDriverContract(contract1, index: 0)
                    }else{
                        let startIndex = contract1.index(contract1.startIndex, offsetBy: 1)
                        self.downloadDriverContract(String(contract1[startIndex...]), index: 0)
                    }
                    
                }
                
                if contract2.count > 1 {
                    
                    let pre = contract2.index(contract2.startIndex, offsetBy: 0)
                    let prefix = String(contract2[pre])
                    
                    if prefix == "c"{
                        self.downloadDriverContract(contract2, index: 0)
                    }else{
                        let startIndex = contract2.index(contract2.startIndex, offsetBy: 1)
                        self.downloadDriverContract(String(contract2[startIndex...]), index: 1)
                    }
                    
                }
                
            }
            break
            
        
        default:
            break
        }
        
    }
    
    override func requestResultFail(_ requestID: String, _ data: Any!) {
        println(">>> requestID = \(requestID)")
        
        showLoadingProgressBar(isShow: false)
        
        
        let dialog = UIAlertController(title: "오류", message: "관리자에게 문의해 주세요.", preferredStyle: .alert)
        let action = UIAlertAction(title: "확인", style: UIAlertAction.Style.default)
        dialog.addAction(action)
        self.present(dialog, animated: true, completion: nil)
        //showPopupView("드라이버 호출 오류", "관리자에게 문의해 주세요.")
    }
}
