//
//  RegisgerViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 07/08/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit

class RegisgerViewController: BaseSwipeViewController {
    @IBOutlet var dimView: UIView!
    @IBOutlet var popupView: UIView!
    
    @IBOutlet var popupTitleLabel: UILabel!
    @IBOutlet var popupContentLabel: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Touch Event 등록 후 함수를 연동한다. (getPosByMap)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dimDidClicked(_:)))
        self.dimView.addGestureRecognizer(gesture)
        
        let childView = self.children[0] as! RegisterTableViewController
        childView.parentDelegate = self
    }
    
    //DimView Clicked
    @objc func dimDidClicked(_ sender: UIGestureRecognizer){
        hidePopupView()
    }
    
    private func showPopupView(){
        popupView.isHidden = false
        dimView.isHidden = false
    }
    private func hidePopupView(){
        popupView.isHidden = true
        dimView.isHidden = true
    }
    // MARK: - Navigation
    @IBAction func clodeDidClicked(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: {})
    }
    
    
    @IBAction func popupConfirmDidClicked(_ sender: Any) {
        hidePopupView()
        
        if popupContentLabel.text == "회원 가입이 완료 되었습니다."{
            self.dismiss(animated: true, completion: {})
        }
    }
    
    /// Delegate
    override func showPopupViewFromDelegate(_ title: String, _ content: String) {
        self.view.endEditing(true)
        popupTitleLabel.text = title
        popupContentLabel.text = content
        popupContentLabel.textColor = UIColorFromRGBA(0x78849E, alpha: 1)
        showPopupView()
    }
    
    override func moveToPagePresentFromDelegate(_ id: String) {
    }
}
