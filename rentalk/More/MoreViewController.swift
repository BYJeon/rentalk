//
//  MoreViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 09/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit

class MoreViewController: BaseSwipeViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let childView = self.children[0] as! MoreTableViewController
        childView.parentDelegate = self
    }
    
    @IBAction func backDidClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    override func moveToPagePresentFromDelegate(_ id: String, title: String, urlInfo: String) {
        if mainOperationDelegate != nil {
            mainOperationDelegate?.moveToPageByPresent?(id: id, title: title, url: urlInfo)
        }
    }
}
