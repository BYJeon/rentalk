//
//  DriverScoreTableViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 21/10/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AWSS3

class DriverScoreTableViewController: BaseTableViewController {
    private let infoManager = InfoManager.getInstance()
    
    //고객 차량 정보
    @IBOutlet var carNumLabel: UILabel!
    
    //요금 내역
    @IBOutlet var serviceFeeLabel: UILabel!
    @IBOutlet var dcFeeLabel: UILabel!
    @IBOutlet var totalFeeLabel: UILabel!
    
    //드라이버 평가
    @IBOutlet var driverImageView: RoundedImageView!
    @IBOutlet var driverNameLabel: UILabel!
    
    @IBOutlet var goodScoreBtn: UIButton!
    @IBOutlet var sosoScoreBtn: UIButton!
    @IBOutlet var badScoreBtn: UIButton!
    
    @IBOutlet var pathInfoView: UIView!
    @IBOutlet var pathViaView: UIView!
    
    //경유지가 없는 경로
    @IBOutlet var targetPath: UITextView!
    @IBOutlet var sourcePath: UITextView!
    @IBOutlet var contractCompleteTimeLabel: UILabel!
    
    @IBOutlet var missionView: UIView!
    
    @IBOutlet var targetTimeLabel: UILabel!
    @IBOutlet var sourceTimeLabel: UILabel!
    
    //경유지가 있는 경로
    @IBOutlet var targetViaPath: UITextView!
    @IBOutlet var viaPath: UITextView!
    @IBOutlet var sourceViaPath: UITextView!
    @IBOutlet var contractViaCompleteTimeLabel: UILabel!
    
    @IBOutlet var targetViaTimeLabel: UILabel!
    @IBOutlet var viaTimeLabel: UILabel!
    @IBOutlet var sourceViaTimeLabel: UILabel!
    @IBOutlet var viaMissionView: UIView!
    
    @IBOutlet var carViaNumLabel: UILabel!
    
    private var missionContent = ""
    
    private var driverHP = ""
    private var empID  = ""
    private var evalScore = "5"
    
    private var isViaMode = false
    
    @IBOutlet var moreEvalTextField: UITextField!
    //AWS S3
    private var downloadProgress : Float = 0
    @objc var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
    
    @objc lazy var transferUtility = {
        AWSS3TransferUtility.default()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        goodScoreBtn.setBackgroundImage(UIImage(named: "btn_ico_good_p"), for: .selected)
        goodScoreBtn.setBackgroundImage(UIImage(named: "btn_ico_good_n"), for: .normal)
        goodScoreBtn.setBackgroundImage(UIImage(named: "btn_ico_good_n"), for: .highlighted)
        goodScoreBtn.isSelected = true
        
        sosoScoreBtn.setBackgroundImage(UIImage(named: "btn_ico_soso_p"), for: .selected)
        sosoScoreBtn.setBackgroundImage(UIImage(named: "btn_ico_soso_n"), for: .normal)
        sosoScoreBtn.setBackgroundImage(UIImage(named: "btn_ico_soso_n"), for: .highlighted)
        
        badScoreBtn.setBackgroundImage(UIImage(named: "btn_ico_bad_p"), for: .selected)
        badScoreBtn.setBackgroundImage(UIImage(named: "btn_ico_bad_n"), for: .normal)
        badScoreBtn.setBackgroundImage(UIImage(named: "btn_ico_bad_n"), for: .highlighted)
        
        request(API_DRIVER_END_INFO_REQUEST)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 8
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2 && !isViaMode {
            return 0
        }else{
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    //좋아요
    @IBAction func goodScoreDidClicked(_ sender: Any) {
        evalScore = "5"
        goodScoreBtn.isSelected = true
        sosoScoreBtn.isSelected = false
        badScoreBtn.isSelected = false
    }
    
    //그저그래요
    @IBAction func sosoScoreDidClicked(_ sender: Any) {
        evalScore = "3"
        sosoScoreBtn.isSelected = true
        goodScoreBtn.isSelected = false
        badScoreBtn.isSelected = false
    }
    
    //나빠요
    @IBAction func badScoreDidClicked(_ sender: Any) {
        evalScore = "1"
        badScoreBtn.isSelected = true
        sosoScoreBtn.isSelected = false
        goodScoreBtn.isSelected = false
    }
    
    //닫기
    @IBAction func closeDidClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //평가 완료
    @IBAction func socreCompleteDidClicked(_ sender: Any) {
        request(API_DIRVER_EVALUATE)
    }
    
    private func downloadDriverProfile(_ imgPath: String){
        if imgPath.count < 2 { return }
        
        let expression = AWSS3TransferUtilityDownloadExpression()
        expression.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                if (self.downloadProgress < Float(progress.fractionCompleted)) {
                    self.downloadProgress = Float(progress.fractionCompleted)
                }
            })
        }
        
        self.completionHandler = { (task, location, data, error) -> Void in
            DispatchQueue.main.async(execute: {
                if let error = error {
                    NSLog("Failed with error: \(error)")
                    //self.statusLabel.text = "Failed"
                }
                else if(self.downloadProgress != 1.0) {
                    //self.statusLabel.text = "Failed"
                }
                else{
                    self.driverImageView.image = UIImage(data: data!)
                }
            })
        }
        
        transferUtility.downloadData(
            forKey: imgPath,
            expression: expression,
            completionHandler: completionHandler).continueWith { (task) -> AnyObject? in
                if let error = task.error {
                    NSLog("Error: %@",error.localizedDescription);
                    DispatchQueue.main.async(execute: {
                        //self.statusLabel.text = "Failed"
                    })
                }
                
                if let _ = task.result {
                    DispatchQueue.main.async(execute: {
                        //self.statusLabel.text = "Downloading..."
                    })
                    NSLog("Download Starting!")
                    // Do something with uploadTask.
                }
                return nil;
        }
    }
    
    //MARK: - network
    override func request(_ name: String) {
        switch name {
        case API_DRIVER_END_INFO_REQUEST: //탁송 완료 시점 정보
            
            let extraParam : String = "/" + infoManager.reqID + "?api_token="+infoManager.getApiToken()
            //let extraParam : String = "/" + infoManager.reqID + "?api_token="+infoManager.getApiToken()
            _ = NetworkManager.sharedInstance.requestGet(name, extraParam, HTTPMethod.get, target: self)
            break
        case API_DIRVER_EVALUATE:
            let apiToken: String = infoManager.getApiToken()
            let reqID: String    = infoManager.reqID
            let empID: String    = infoManager.getAppID()
            
            let detail: String = moreEvalTextField.trimmingCharacters()
            let params: Parameters = [
                "api_token" : apiToken
               ,"req_id" : reqID
               ,"driver_id" : driverHP
               ,"emp_id" : empID
               ,"eval_score" : evalScore
               ,"eval_detail" : detail
            ]
            _ = NetworkManager.sharedInstance.request(name, params, HTTPMethod.post, target: self)
            break
            
        default:
            break
        }
    }
    
    
    
    override func requestFinished(_ requestID: String, _ data: Any!) {
        switch requestID {
            case API_DRIVER_END_INFO_REQUEST:
                let json = JSON(data!)
                if json["success"].stringValue == "0000" {
                    
                    let way_point_addr   = json["waypoint_info"]["address"].stringValue
                    let start_point_addr = json["start_point_info"]["address"].stringValue
                    let end_point_addr   = json["end_point_info"]["address"].stringValue
                    
                    let way_point_time = json["waypoint_info"]["arrive_time"].stringValue
                    let start_point_time = json["start_point_info"]["arrive_time"].stringValue
                    let end_point_time = json["end_point_info"]["arrive_time"].stringValue
                    
                    missionContent = json["mission_info"][0]["mission_nm"].stringValue
                    
                    if way_point_addr.count != 0 {
                        isViaMode = true
                        pathViaView.isHidden = false
                        pathInfoView.isHidden = true
                        targetViaPath.text = start_point_addr
                        sourceViaPath.text = end_point_addr
                        viaPath.text = way_point_addr
                        
                        targetViaTimeLabel.text = start_point_time
                        sourceViaTimeLabel.text = end_point_time
                        viaTimeLabel.text    = way_point_time
                        
                        if missionContent.count > 0 {
                            missionView.isHidden = true
                            viaMissionView.isHidden = false
                            contractViaCompleteTimeLabel.isHidden = false
                            contractViaCompleteTimeLabel.text = json["mission_info"][0]["completed_time"].stringValue
                        }else{
                            missionView.isHidden = true
                            viaMissionView.isHidden = true
                            contractViaCompleteTimeLabel.isHidden = true
                            contractCompleteTimeLabel.isHidden = true
                        }
                        
                    }else{
                        isViaMode = false
                        pathViaView.isHidden = true
                        pathInfoView.isHidden = false
                        
                        targetPath.text = start_point_addr
                        sourcePath.text = end_point_addr
                        
                        targetTimeLabel.text = start_point_time
                        sourceTimeLabel.text = end_point_time
                        
                        if missionContent.count > 0 {
                            missionView.isHidden = false
                            viaMissionView.isHidden = true
                            contractCompleteTimeLabel.isHidden = false
                            contractCompleteTimeLabel.text = json["mission_info"][0]["completed_time"].stringValue
                        }else{
                            missionView.isHidden = true
                            viaMissionView.isHidden = true
                            contractViaCompleteTimeLabel.isHidden = true
                            contractCompleteTimeLabel.isHidden = true
                        }
                    }
                    
                    
                    //driverIDLabel.text = json["req_id"].stringValue
                    driverNameLabel.text = json["driver_info"]["driver_nm"].stringValue
                    driverHP = json["driver_info"]["tel_no"].stringValue
                    serviceFeeLabel.text = json["fee_info"]["service_fee"].stringValue.getCurrencyString() + " 원"
                    dcFeeLabel.text = json["fee_info"]["total_dc"].stringValue.getCurrencyString() + " 원"
                    totalFeeLabel.text = json["fee_info"]["charge_fee"].stringValue.getCurrencyString() + " 원"
                    
                    
                    let profile  = json["driver_info"]["profile_photo"].stringValue
                    let pathInfo = json["path_img"].stringValue
                    
                    driverNameLabel.text = json["driver_info"]["driver_nm"].stringValue
                    carNumLabel.text = json["car_info"]["car_no"].stringValue
                    carViaNumLabel.text = json["car_info"]["renter_car_no"].stringValue
                    //carNameLabel.text = json["car_info"]["car_mdl"].stringValue
                    //driverNumber = json["driver_info"]["tel_no"].stringValue
                    if profile.count > 1 {
                        downloadDriverProfile(profile)
                    }
                    
                    self.tableView.reloadData()
                }
                break
            case API_DIRVER_EVALUATE:
                let json = JSON(data!)
                if json["success"].stringValue == "0000" {
                    self.dismiss(animated: true, completion: nil)
                }
                break
            default:
                break
        }
    }
}
