//
//  NSObject+TmapAPI.h
//  rentalk
//
//  Created by Yuna Daddy on 06/08/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMapView.h"

NS_ASSUME_NONNULL_BEGIN
#define TMAP_APPKEY @"36ed77a4-99ac-4b6d-95e7-c55123f54ae0"

TMapView*       _mapView;

@interface TmapAPI : NSObject

-(void)createMapView;
-(NSArray*)getROIList:(NSString*) searchWord;
-(NSString*)getAddrFromGpsPoint:(double)lat lng:(double)lng;
-(NSDictionary*)getPathDistanceInfo:(double)lat lng:(double)lng slat:(double)slat slng:(double)slng;
-(NSDictionary*)getPathDistanceInfoWithVia:(double)lat lng:(double)lng
                                       slat:(double)slat slng:(double)slng
                                       vialat:(double)vialat vialng:(double)vialng;
@end

NS_ASSUME_NONNULL_END
