//
//  ContractImage.swift
//  rentalk
//
//  Created by Yuna Daddy on 10/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//
import UIKit
import NYTPhotoViewer

class ContractImage: NSObject, NYTPhoto {
    
    var image: UIImage?
    var imageData: Data?
    var placeholderImage: UIImage?
    var attributedCaptionTitle: NSAttributedString?
    var attributedCaptionSummary: NSAttributedString?
    var attributedCaptionCredit: NSAttributedString?
    
    init(image: UIImage? = nil, imageData: NSData? = nil) {
        super.init()
        
        self.image = image
        self.imageData = imageData as Data?
    }
}
