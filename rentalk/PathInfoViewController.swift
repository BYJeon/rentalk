//
//  AddrViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 06/08/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import Mixpanel

enum PathType {
    case TARGET
    case SOURCE
    case REPAIR_SHOP
}

class PathInfoViewController: BaseSwipeViewController, UnderLineTextFieldProtocol, PathManagerDelegate, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet var targetPathImg: UIImageView!
    @IBOutlet var targetTextField: UnderlineTextField!
    
    @IBOutlet var sourcePathImg: UIImageView!
    @IBOutlet var sourceTextField: UnderlineTextField!
    
    @IBOutlet var repairShopPathImg: UIImageView!
    @IBOutlet var repairShopTextField: UnderlineTextField!
    @IBOutlet var repairShopLabel: UILabel!
    @IBOutlet var repairPathView: UIView!
    
    @IBOutlet var repairShopAddBtn: UIButton!
    @IBOutlet var repairShopRemoveBtn: UIButton!
    
    //지도에서 위치 설정하기
    @IBOutlet var posMapView: UIView!
    @IBOutlet var naverMapView: BaseMapView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var completePathBtn: RoundButton!
    
    
    private var addrList : NSArray = []
    private let tmapAPI = TmapAPI()
    private let pathInfoManager = PathInfoManager.sharedInstance
    
    private var bTargetSelected : Bool = false
    private var bSrouceSelected : Bool = false
    private var bRepairShopSelected : Bool = false
    private var pathType = PathType.SOURCE
    
    @IBOutlet var repairShopView: UIView!
    
    private var isViaMode = false
    private var isLoaded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        targetTextField.delegateTextField = self
        sourceTextField.delegateTextField = self
        repairShopTextField.delegateTextField = self
        
        // Touch Event 등록 후 함수를 연동한다. (getPosByMap)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.getPosByMap(_:)))
        self.posMapView.addGestureRecognizer(gesture)
        
        let gestureAddRepairShop = UITapGestureRecognizer(target: self, action: #selector(self.addRepairShop(_:)))
        self.repairShopLabel.addGestureRecognizer(gestureAddRepairShop)
        
        naverMapView.setInfoDelegate(self)
        
        let nibName = UINib(nibName: "AddrCustomCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "AddrCell")
        
        if InfoManager.getInstance().companyTypeGb != "1"{
           repairShopView.isHidden = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addrList = []
        tableView.reloadData()
        
        pathInfoManager.clearPathItem()
        
        if pathInfoManager.myLocationInfo == nil {return}
        // 나의 위치를 시작점으로 한다.
        let latitude = pathInfoManager.myLocationInfo?.coordinate.latitude
        let longitude = pathInfoManager.myLocationInfo?.coordinate.longitude
            
        let address : String = tmapAPI.getAddrFromGpsPoint(latitude!, lng: longitude!)
        if isTargetPosEnable(address){
            let pathItem = PathItem(lat: latitude! , lng: longitude!, address: address)
            pathInfoManager.setPathInfo(item: pathItem, pathType: .TARGET, isViaMode: false)
            
            // 시작 지점은 현재 위치 기반으로 정해 진다.
            targetPathImg.image = UIImage(named: "ico_map_location_p")
            let targetPath = pathInfoManager.getTargetInfo();
            targetTextField.text = targetPath?.getAddress()
            
            pathType = PathType.SOURCE
            sourceTextField.becomeFirstResponder()
            
        }else{
            targetPathImg.image = UIImage(named: "ico_map_location_n")
            targetTextField.text = "현재 서비스 지역이 아닙니다."
            pathInfoManager.targetPathItem = nil
            pathType = PathType.TARGET
            targetTextField.becomeFirstResponder()
        }
       
        
        if mainOperationDelegate != nil {
            mainOperationDelegate?.setRequestPathToMap()
        }
    }
    
    // 출발지 필터
    func filterTargetPos(_ arr : NSArray ) -> NSArray{
        var pos : [TMapPOIItem] = []
        
        for i in 0 ..< arr.count{
            let item : TMapPOIItem = arr[i] as! TMapPOIItem
            if isTargetPosEnable(item) {
                pos.append(item)
            }
        }
        
        return pos as NSArray
    }
    
    // 도착지 필터
    func filterSourcePos(_ arr : NSArray ) -> NSArray{
        var pos : [TMapPOIItem] = []
        
        for i in 0 ..< arr.count{
            let item : TMapPOIItem = arr[i] as! TMapPOIItem
            if isSourcePosEnable(item) {
                pos.append(item)
            }
        }
        
        return pos as NSArray
    }
    
    func isTargetPosEnable(_ addr : String) -> Bool {
        #if DEBUG
        return true
        #endif
            
        if(addr.contains("서울") || addr.contains("경기")  || addr.contains("대구") || addr.contains("대전") || addr.contains("부산")){
                   return true
        }else if(addr.contains("인천")) {
            if(addr.contains("중구")) {
                return false
            } else {
                return true
            }
        } else if(addr.contains("강원")) {
            if(addr.contains("원주")) {
                return true;
            } else {
                return false;
            }
        } else if(addr.contains("충북") || addr.contains("충청북도")) {
            if(addr.contains("청주")) {
                return true;
            } else {
                return false;
            }
        }

        return true
        //return addr.contains("서울") /*&& ( addr.contains("광진구") || addr.contains("동대문구") || addr.contains("성동구") )*/
    }

    func isSourcePosEnable(_ addr : String) -> Bool {
        #if DEBUG
        return true
        #endif
        
        if(addr.contains("제주")){
            return false
        }else if(addr.contains("인천")) {
            if(addr.contains("중구") || addr.contains("옹진")) {
                return false
            } else {
                return true
            }
        } else if(addr.contains("경북") || addr.contains("경상북")) {
            if(addr.contains("울릉")) {
                return false
            } else {
                return true
            }
        }

        return true;
        
        
        //return addr.contains("서울")
    }
    
    func isTargetPosEnable(_ item : TMapPOIItem) -> Bool {
        #if DEBUG
        return true
        #endif
        
        if(item.upperAddrName.contains("서울") || item.upperAddrName.contains("경기")  || item.upperAddrName.contains("대구") || item.upperAddrName.contains("대전") || item.upperAddrName.contains("부산")){
            return true
        }else if(item.upperAddrName.contains("인천")) {
            if(item.middleAddrName.contains("중구")) {
                return false;
            } else {
                return true;
            }
        } else if(item.upperAddrName.contains("강원")) {
            if(item.middleAddrName.contains("원주")) {
                return true;
            } else {
                return false;
            }
        } else if(item.upperAddrName.contains("충북") || item.upperAddrName.contains("충청북도")) {
            if(item.middleAddrName.contains("청주")) {
                return true;
            } else {
                return false;
            }
        }

        return false
        
        //return item.upperAddrName == "서울" /*&& (item.middleAddrName == "광진구" || item.middleAddrName == "동대문구" || item.middleAddrName == "성동구")*/
    }
    
    func isSourcePosEnable(_ item : TMapPOIItem) -> Bool {
        #if DEBUG
        return true
        #endif
        
        if(item.upperAddrName.contains("제주")){
            return false
        }else if(item.upperAddrName.contains("인천")) {
            if(item.middleAddrName.contains("중구") || item.middleAddrName.contains("옹진")) {
                return false;
            } else {
                return true;
            }
        } else if(item.upperAddrName.contains("경북") || item.upperAddrName.contains("경상북")) {
            if(item.middleAddrName.contains("울릉")) {
                return false;
            } else {
                return true;
            }
        }

        return true;
        
        //return item.upperAddrName == "서울"
    }
    
    //Mark : UnderlineTextField Delageta
    func textValueChanged(sender: UnderlineTextField) {
        //차고지 주소 입력
        if sender == targetTextField {
            if targetTextField.text?.count == 0 {
                bTargetSelected = false
            }
            
            addrList = filterTargetPos(tmapAPI.getROIList(targetTextField.text ?? "") as NSArray)
        }
        
        //임차인 주소 입력
        if sender == sourceTextField {
            if sourceTextField.text?.count == 0 {
                bSrouceSelected = false
            }
            
            addrList = filterSourcePos(tmapAPI.getROIList(sourceTextField.text ?? "") as NSArray)
        }
        
        //임차인 주소 입력
        if sender == repairShopTextField {
            if repairShopTextField.text?.count == 0 {
                bRepairShopSelected = false
            }
            
            addrList = filterSourcePos(tmapAPI.getROIList(repairShopTextField.text ?? "") as NSArray)
        }
        
        
        isLoaded = true
        tableView.reloadData()
    }
    
    
    func textValueDidBegin(sender: UnderlineTextField) {
        naverMapView.fadeOut()
        
        let targetPath = pathInfoManager.getTargetInfo()
        let sourcePath = pathInfoManager.getSourceInfo()
        let viaPath    = pathInfoManager.getRepariShopInfo()
        //차고지 주소 입력
        if sender == targetTextField {
            targetPathImg.image = UIImage(named: "ico_map_location_p")
            pathType = PathType.TARGET
            
            if targetPath != nil {
                targetTextField.text = targetPath?.getAddress()
            }
            
            checkPathImgByInfo()
        }
        
        //임차인 주소 입력
        if sender == sourceTextField {
            sourcePathImg.image = UIImage(named: isViaMode ? "ico_map_location02_p" : "ico_map_location03_p")
            pathType = PathType.SOURCE
            
            targetPathImg.image = UIImage(named: targetPath == nil ? "ico_map_location_n" : "ico_map_location_p")
            
            checkPathImgByInfo()
        }
        
        if sender == repairShopTextField {
            repairShopPathImg.image = UIImage(named: "ico_map_location03_p")
            pathType = PathType.REPAIR_SHOP
            
            targetPathImg.image = UIImage(named: targetPath == nil ? "ico_map_location_n" : "ico_map_location_p")
            
            checkPathImgByInfo()
        }
    }
    
    //정보를 기반으로 경로 이미지 표시
    func checkPathImgByInfo(){
        let targetPath = pathInfoManager.getTargetInfo()
        let sourcePath = pathInfoManager.getSourceInfo()
        let viaPath    = pathInfoManager.getRepariShopInfo()
        
        if isViaMode{
            if viaPath != nil {
                sourcePathImg.image = UIImage(named: isViaMode ? "ico_map_location02_p" : "ico_map_location03_p")
            }else{
                sourcePathImg.image = UIImage(named: isViaMode ? "ico_map_location02_n" : "ico_map_location03_n")
            }
            
            if sourcePath != nil {
                repairShopPathImg.image = UIImage(named: isViaMode ? "ico_map_location03_p" : "ico_map_location02_p")
            }else{
                repairShopPathImg.image = UIImage(named: isViaMode ? "ico_map_location03_n" : "ico_map_location02_n")
            }
        }else{
            if sourcePath != nil {
                sourcePathImg.image = UIImage(named: isViaMode ? "ico_map_location02_p" : "ico_map_location03_p")
            }else{
                sourcePathImg.image = UIImage(named: isViaMode ? "ico_map_location02_n" : "ico_map_location03_n")
            }
            
            if viaPath != nil {
                repairShopPathImg.image = UIImage(named: isViaMode ? "ico_map_location02_p" : "ico_map_location03_p")
            }else{
                repairShopPathImg.image = UIImage(named: isViaMode ? "ico_map_location02_n" : "ico_map_location03_n")
            }
        }
    }
    
    func textShouldReturn(sender: UnderlineTextField) {
        let targetPath = pathInfoManager.getTargetInfo();
        let sourcePath = pathInfoManager.getSourceInfo()
        let viaPath = pathInfoManager.getRepariShopInfo()
        
        //차고지 주소 입력
        if sender == targetTextField {
            pathType = PathType.TARGET
//            if targetTextField.text?.count == 0 {
//                bTargetSelected = false
//                pathInfoManager.setPathInfo(item: nil, pathType: PathType.TARGET)
//                targetPathImg.image = UIImage(named: "ico_map_location_n")
//            }
            if targetPath != nil {
                targetTextField.text = targetPath?.getAddress()
            }else{
                targetTextField.text = ""
                targetPathImg.image = UIImage(named: "ico_map_location_n")
            }
        }
        
        //임차인 주소 입력
        if sender == sourceTextField {
            pathType = PathType.SOURCE
//            if sourceTextField.text?.count == 0 {
//                bSrouceSelected = false
//                pathInfoManager.setPathInfo(item: nil, pathType: PathType.SOURCE)
//                sourcePathImg.image = UIImage(named: "ico_map_location_n")
//            }
            
            if isViaMode {
                if viaPath != nil {
                    sourceTextField.text = viaPath?.getAddress()
                }else {
                    sourceTextField.text = ""
                    sourcePathImg.image = UIImage(named: "ico_map_location02_n")
                }
            }else{
                if sourcePath != nil {
                    sourceTextField.text = sourcePath?.getAddress()
                }else {
                    sourceTextField.text = ""
                    sourcePathImg.image = UIImage(named: "ico_map_location03_n")
                }
            }
            
        }
        
        if sender == repairShopTextField {
            pathType = PathType.REPAIR_SHOP
            if repairShopTextField.text?.count == 0 {
                bRepairShopSelected = false
                pathInfoManager.setPathInfo(item: nil, pathType: PathType.REPAIR_SHOP, isViaMode: isViaMode)
                repairShopPathImg.image = UIImage(named: "ico_map_location_n")
            }
        }
        
        addrList = []
        tableView.reloadData()
        
        if isViaMode {
            completePathBtn.isHidden = !(targetPath != nil && sourcePath != nil && viaPath != nil)
        }else{
            completePathBtn.isHidden = !(targetPath != nil && sourcePath != nil)
        }
    }
    
    //Mark : View Event
    @IBAction func completeDidClicked(_ sender: Any) {
        #if RELEASE
        Mixpanel.mainInstance().track(event: "SET_CHECKOUT_OPTION",
                                      properties:["CHECKOUT_STEP": "2", "CHECKOUT_OPTION": "TYPE"])
        #endif
        
        //도착지 정보를 저장 한다.
        let sourceInfo = pathInfoManager.getSourceInfo()
        pathInfoManager.addAddrItem(item: AddrHistoryItem(poiName: sourceInfo!.getPoiName(), addr: (sourceInfo?.getAddress())!,
                                                          lat: sourceInfo?.getLatInfo() ?? 0 , lgn: sourceInfo?.getLngInfo() ?? 0))
        
        InfoManager.getInstance().setMenuType(type: .SET_REQUEST_PATH)
        if mainOperationDelegate != nil{
            mainOperationDelegate?.setRequestPathToMap()
        }
        navigationController?.popViewController(animated: true)
    }
    // 뒤로 가기 버튼 클릭
    @IBAction func backDidClicked(_ sender: Any) {
        pathInfoManager.setPathInfo(item: nil, pathType: .SOURCE, isViaMode: false)
        
        navigationController?.popViewController(animated: true)
    }
    
    //공업사 추가
    @IBAction func repairShopAddDidClicked(_ sender: Any) {
        isViaMode = true
        
        repairShopAddBtn.isHidden = false
        repairShopTextField.isHidden = false
        repairShopAddBtn.isHidden = true
        repairShopRemoveBtn.isHidden = false
        repairPathView.isHidden = false
        repairShopPathImg.isHidden = false
        repairShopLabel.isHidden = true
        
        sourceTextField.placeholder = "경유(임차인) 주소 입력하기"
        repairShopTextField.placeholder = "도착지 주소 입력하기"
        
        //경유지 추가 시 목적시 삭제 함
        //pathInfoManager.sourcePathItem = nil
        
        let sourcePath = pathInfoManager.getSourceInfo()
        if sourcePath != nil {
            repairShopTextField.text = sourcePath?.getAddress()
            if repairShopTextField.text!.count > 0 {
                sourcePathImg.image = UIImage(named: "ico_map_location02_p")
            }else{
                sourcePathImg.image = UIImage(named: "ico_map_location02_n")
            }
        }else{
            sourcePathImg.image = UIImage(named: "ico_map_location02_n")
        }
        
        
        sourceTextField.text = ""
        sourceTextField.becomeFirstResponder()
    }
    
    //공업사 주소 제거
    @IBAction func repairShopRemoveDidClicked(_ sender: Any) {
        isViaMode = false
        
        repairShopAddBtn.isHidden = true
        repairShopTextField.isHidden = true
        repairShopAddBtn.isHidden = false
        repairShopRemoveBtn.isHidden = true
        repairPathView.isHidden = true
        repairShopPathImg.isHidden = true
        repairShopLabel.isHidden = false
        
        pathInfoManager.repairShopPathItem = nil
        
        let sourcePath = pathInfoManager.getSourceInfo()
        if sourcePath != nil {
            sourceTextField.text = sourcePath?.getAddress()
            if sourceTextField.text!.count > 0 {
                sourcePathImg.image = UIImage(named: "ico_map_location03_p")
            }else{
                sourcePathImg.image = UIImage(named: "ico_map_location03_n")
            }
        }else{
            sourcePathImg.image = UIImage(named: "ico_map_location03_n")
        }
        
        repairShopTextField.text = ""
        //sourceTextField.text = ""
        //sourceTextField.placeholder = "도착지 주소 입력하기"
        repairShopTextField.placeholder = "경유(임차인) 주소 입력하기"
    }
 
    //MARK :  추가 함수
    @objc func getPosByMap(_ sender: UIGestureRecognizer){
        targetTextField.resignFirstResponder()
        sourceTextField.resignFirstResponder()
        repairShopTextField.resignFirstResponder()
        
        if naverMapView.isHidden == true {
            completePathBtn.isHidden = true
            naverMapView.fadeIn()
        }else{
            naverMapView.fadeOut()
        }
    }
    
    //경유지 추가 하기
    @objc func addRepairShop(_ sender: UIGestureRecognizer){
        isViaMode = true
        
        repairShopAddBtn.isHidden = false
        repairShopTextField.isHidden = false
        repairShopAddBtn.isHidden = true
        repairShopRemoveBtn.isHidden = false
        repairPathView.isHidden = false
        repairShopPathImg.isHidden = false
        repairShopLabel.isHidden = true
        
        sourceTextField.placeholder = "경유(임차인) 주소 입력하기"
        repairShopTextField.placeholder = "도착지 주소 입력하기"
        
        //경유지 추가 시 목적시 삭제 함
        //pathInfoManager.sourcePathItem = nil
        
        let sourcePath = pathInfoManager.getSourceInfo()
        if sourcePath != nil {
            repairShopTextField.text = sourcePath?.getAddress()
            if repairShopTextField.text!.count > 0 {
                sourcePathImg.image = UIImage(named: "ico_map_location02_p")
            }else{
                sourcePathImg.image = UIImage(named: "ico_map_location02_n")
            }
        }else{
            sourcePathImg.image = UIImage(named: "ico_map_location02_n")
        }
        
        sourceTextField.text = ""
        sourceTextField.becomeFirstResponder()
    }
    
    //MARK :  BaseMapView Delegate
    func displayAddrByMap(lat: Double, lng: Double) {
        if naverMapView.isHidden { return }
        let address : String = tmapAPI.getAddrFromGpsPoint(lat, lng: lng)
        
        if pathType == PathType.TARGET {
            targetTextField.text = address
        }else if pathType == PathType.SOURCE{
            sourceTextField.text = address
        }else if pathType == PathType.REPAIR_SHOP{
            repairShopTextField.text = address
        }
    }
    
    func selectedAddrByMap(lat: Double, lng: Double) {
        let address : String = tmapAPI.getAddrFromGpsPoint(lat, lng: lng)
        let pathItem = PathItem(lat: lat , lng: lng, address: address)
        
        if pathType == PathType.TARGET {
            if isTargetPosEnable(address){
                bTargetSelected = true
                targetTextField.text = address
            }else{
                bTargetSelected = false
                targetTextField.text = "현재 서비스 지역이 아닙니다."
                pathInfoManager.targetPathItem = nil
                naverMapView.fadeOut()
                return
            }
            
        }else if pathType == PathType.SOURCE{
            if isSourcePosEnable(address){
                bSrouceSelected = true
                sourceTextField.text = address
            }else{
                bSrouceSelected = false
                sourceTextField.text = "현재 서비스 지역이 아닙니다."
                pathInfoManager.sourcePathItem = nil
                naverMapView.fadeOut()
                return
            }
            
        }else if pathType == PathType.REPAIR_SHOP{
            if isSourcePosEnable(address){
                bRepairShopSelected = true
                repairShopTextField.text = address
            }else{
                bRepairShopSelected = false
                repairShopTextField.text = "현재 서비스 지역이 아닙니다."
                pathInfoManager.repairShopPathItem = nil
                naverMapView.fadeOut()
                return
            }
            
        }
        
        pathInfoManager.setPathInfo(item: pathItem, pathType: pathType, isViaMode: isViaMode)
        naverMapView.fadeOut()
        
        checkPathImgByInfo()
        
        let targetPath = pathInfoManager.getTargetInfo()
        let sourcePath = pathInfoManager.getSourceInfo()
        let viaPath = pathInfoManager.getRepariShopInfo()
        
        if isViaMode {
            completePathBtn.isHidden = !(targetPath != nil && sourcePath != nil && viaPath != nil)
        }else{
            completePathBtn.isHidden = !(targetPath != nil && sourcePath != nil)
        }
    }
    
    // MARK : TableViewr
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isLoaded == false ? pathInfoManager.addrHistoryArr.count : addrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddrCell", for: indexPath) as! AddrCustomCell
        
        //주소 이전 History 검색
        if isLoaded == false{
            let item = pathInfoManager.addrHistoryArr[ pathInfoManager.addrHistoryArr.count - indexPath.row - 1]
            cell.contentLabel.text = item.poiName
            cell.addrLabel.text = item.addr
            cell.selectionStyle = .none
        }else{
            let item = addrList[indexPath.row] as! TMapPOIItem
            cell.contentLabel.text = item.poiName
            cell.addrLabel.text = pathInfoManager.getAddrFromTmapAPI(item)
            cell.selectionStyle = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var pathItem: PathItem! = nil
        var address: String! = ""
        
        if isLoaded == false{
            let item = pathInfoManager.addrHistoryArr[pathInfoManager.addrHistoryArr.count - indexPath.row - 1]
            address = item.addr
            pathItem = PathItem(lat: Double(item.lat ?? 0) , lng: Double(item.lgn ?? 0), address: item.addr, poiName: item.poiName)
            
        }else{
            let item = addrList[indexPath.row] as! TMapPOIItem
            address  = pathInfoManager.getAddrFromTmapAPI(item)
            pathItem = PathItem(lat: Double(item.getPOIPoint()?.coordinate.latitude ?? 0) , lng: Double(item.getPOIPoint()?.coordinate.longitude ?? 0), address: address, poiName: item.getPOIName())
        }
        if pathType == PathType.TARGET {
            bTargetSelected = true
            targetTextField.text = address
            targetPathImg.image = UIImage(named: "ico_map_location_p")
        }else if pathType == PathType.SOURCE{
            bSrouceSelected = true
            sourceTextField.text = address
            sourcePathImg.image = UIImage(named: isViaMode ? "ico_map_location02_p" : "ico_map_location03_p")
            
        }else if pathType == PathType.REPAIR_SHOP{
            bRepairShopSelected = true
            repairShopTextField.text = address
            repairShopPathImg.image = UIImage(named: "ico_map_location03_p")
        }
        
        pathInfoManager.setPathInfo(item: pathItem, pathType: pathType, isViaMode: isViaMode)
        
        let targetPath = pathInfoManager.getTargetInfo()
        let sourcePath = pathInfoManager.getSourceInfo()
        
        completePathBtn.isHidden = !(targetPath != nil && sourcePath != nil)
        self.view.endEditing(true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension UIView{
    func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
        self.alpha = 0.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.isHidden = false
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
        self.alpha = 1.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }) { (completed) in
            self.isHidden = true
            completion(true)
        }
    }}
