//
//  DriverScoreViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 18/10/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit

class DriverScoreViewController: BaseSwipeViewController {
    private var statusBarView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        if #available(iOS 13.0, *) {
            let tag = 38482458385
            if let statusBar = UIApplication.shared.keyWindow?.viewWithTag(tag) {
                statusBarView = statusBar
            } else {
                let statusBar = UIView(frame: UIApplication.shared.statusBarFrame)
                statusBar.tag = tag
                UIApplication.shared.keyWindow?.addSubview(statusBar)
                statusBarView = statusBar
            }
        } else {
            statusBarView = UIApplication.shared.value(forKey: "statusBar") as? UIView
        }
        
        statusBarView!.backgroundColor = UIColor.colorWithRGBHex(hex: 0x00A486, alpha: 1)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        statusBarView!.backgroundColor = UIColor.clear
    }
}
