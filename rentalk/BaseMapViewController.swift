//
//  BaseMapViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 07/08/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit
import NMapsMap

class BaseMapViewController: BaseSwipeViewController, NMFMapViewDelegate {
    internal var mapView : NMFMapView? = nil
    
    internal let tmapAPI = TmapAPI()
    open var driverRedID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //custom initialization
        mapView = NMFMapView(frame: self.view.frame)
        mapView?.delegate = self
        self.view.insertSubview(mapView!, at: 0)
    }
}

extension UIView {
    
    func takeScreenshot(_ bottomGap: CGFloat) -> UIImage {
        // Begin context
        let bounds = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height-bottomGap - getNumber(900))
        let targetBounds = CGRect(x: 0, y: getNumber(-600), width: self.bounds.width, height: self.bounds.height)
        //self.bounds.size = CGSize(width: self.bounds.width, height: self.bounds.height - 300)
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: targetBounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
}
