//
//  CustomerStartViewController.swift
//  rentalk
//
//  Created by Yuna Daddy on 05/09/2019.
//  Copyright © 2019 rentalk. All rights reserved.
//

import UIKit

class CustomerStartViewController: BaseSwipeViewController{
    @IBOutlet var dimView: UIView!
    @IBOutlet var popupView: UIView!
    @IBOutlet var popupTitleLabel: UILabel!
    @IBOutlet var popupContentLabel: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Touch Event 등록 후 함수를 연동한다. (getPosByMap)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dimDidClicked(_:)))
        self.dimView.addGestureRecognizer(gesture)
        
        let childView = self.children[0] as! BaseTableViewController
        childView.parentDelegate = self
        
        //드라이버 찾을 수 없음 noti
        //NotificationCenter.default.addObserver(self, selector: #selector(driverNotFound), name: NSNotification.Name(rawValue: NOTI_DRIVER_NOT_FOUND), object: nil)
    }
    
    @objc func driverNotFound() {
        showPopupViewFromDelegate("탁송요청 실패", "수행가능한 드라이버가 없습니다.")
    }
    
    //DimView Clicked
    @objc func dimDidClicked(_ sender: UIGestureRecognizer){
        hidePopupView()
    }
    
    @IBAction func popupConfirmDidClicked(_ sender: Any) {
        hidePopupView()
    }
    
    private func showPopupView(){
        popupView.isHidden = false
        dimView.isHidden = false
    }
    private func hidePopupView(){
        popupView.isHidden = true
        dimView.isHidden = true
    }
    
    /// Delegate 
    override func showPopupViewFromDelegate(_ title: String, _ content: String) {
        popupTitleLabel.text = title
        popupContentLabel.text = content
        popupContentLabel.textColor = UIColorFromRGBA(0x78849E, alpha: 1)
        showPopupView()
    }
    
    override func moveToPagePresentFromDelegate(_ id: String) {
        if mainOperationDelegate != nil {
            mainOperationDelegate?.moveToPageByPresent(id: id)
        }
    }
}
